package com.pucp.edu.pe.votingsystem.payload.request.Elections;

public class RegisterElectionRequest {
    private String name;
    private String description;
    private String logo;
    private String status;
    private String descriptionInscriptions;
    private String startDateInscriptions;
    private String endDateInscriptions;
    private String descriptionActualization;
    private String startDateActualization;
    private String endDateActualization;
    private String descriptionVote;
    private String startDateVote;
    private String endDateVote;
    private Integer creationUser;

    public RegisterElectionRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescriptionInscriptions() {
        return descriptionInscriptions;
    }

    public void setDescriptionInscriptions(String descriptionInscriptions) {
        this.descriptionInscriptions = descriptionInscriptions;
    }

    public String getStartDateInscriptions() {
        return startDateInscriptions;
    }

    public void setStartDateInscriptions(String startDateInscriptions) {
        this.startDateInscriptions = startDateInscriptions;
    }

    public String getEndDateInscriptions() {
        return endDateInscriptions;
    }

    public void setEndDateInscriptions(String endDateInscriptions) {
        this.endDateInscriptions = endDateInscriptions;
    }

    public String getDescriptionActualization() {
        return descriptionActualization;
    }

    public void setDescriptionActualization(String descriptionActualization) {
        this.descriptionActualization = descriptionActualization;
    }

    public String getStartDateActualization() {
        return startDateActualization;
    }

    public void setStartDateActualization(String startDateActualization) {
        this.startDateActualization = startDateActualization;
    }

    public String getEndDateActualization() {
        return endDateActualization;
    }

    public void setEndDateActualization(String endDateActualization) {
        this.endDateActualization = endDateActualization;
    }

    public String getDescriptionVote() {
        return descriptionVote;
    }

    public void setDescriptionVote(String descriptionVote) {
        this.descriptionVote = descriptionVote;
    }

    public String getStartDateVote() {
        return startDateVote;
    }

    public void setStartDateVote(String startDateVote) {
        this.startDateVote = startDateVote;
    }

    public String getEndDateVote() {
        return endDateVote;
    }

    public void setEndDateVote(String endDateVote) {
        this.endDateVote = endDateVote;
    }

    public Integer getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(Integer creationUser) {
        this.creationUser = creationUser;
    }
}
