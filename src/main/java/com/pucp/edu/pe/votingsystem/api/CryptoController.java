package com.pucp.edu.pe.votingsystem.api;

import com.google.gson.Gson;
import com.pucp.edu.pe.votingsystem.dto.User;
import com.pucp.edu.pe.votingsystem.dto.Vote;
import com.pucp.edu.pe.votingsystem.payload.request.Crypto.CryptoRequest;
import com.pucp.edu.pe.votingsystem.payload.request.Elections.VoteRequest;
import com.pucp.edu.pe.votingsystem.payload.request.IdRequest;
import com.pucp.edu.pe.votingsystem.security.Crypto.CryptoUtil;
import com.pucp.edu.pe.votingsystem.service.UserService;
import com.pucp.edu.pe.votingsystem.service.VoteService;
import org.apache.tomcat.util.buf.HexUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transaction;
import javax.validation.Valid;
import java.net.CacheRequest;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.Base64;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/crypto")
public class CryptoController {
    @Autowired
    UserService userService;

    @Autowired
    VoteService voteService;

    @Autowired
    PasswordEncoder encoder;

    @PostMapping(path = "/publickey")
    public @ResponseBody
    User getPublicKey(@Valid @RequestBody IdRequest idRequest) throws Exception {
        User user = userService.findById(idRequest.getId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho usuario."));

        //server generates RSA key pair - public and private keys
        generateRsaKeyPair(user);

        userService.save(user);

        //to simplify our example, User object is returned with generated RSA public key
        //RSA private key is not included in response because it should be kept as secret
        return user;
    }

    private void generateRsaKeyPair(User user) throws NoSuchAlgorithmException {
        KeyPair keyPair = CryptoUtil.generateRsaKeyPair();

        byte[] publicKey = keyPair.getPublic().getEncoded();
        byte[] privateKey = keyPair.getPrivate().getEncoded();

        //encoding keys to Base64 text format so that we can send public key via REST API
        String rsaPublicKeyBase64 = new String(Base64.getEncoder().encode(publicKey));
        String rsaPrivateKeyBase64 = new String(Base64.getEncoder().encode(privateKey));

        //saving keys to user object for later use
        user.setRsaPublicKey(rsaPublicKeyBase64);
        user.setRsaPrivateKey(rsaPrivateKeyBase64);

    }

    @PostMapping(path = "/transaction")
    public ResponseEntity<?> doTransaction(@RequestBody CryptoRequest encryptedTransaction) throws Exception {
        User user = userService.findById(Integer.parseInt(encryptedTransaction.getUserId()))
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho usuario."));

        String encAesKeyBase64 = encryptedTransaction.getEncAesKey();
        //decode from Base64 format
        byte[] encAesKeyBytes = Base64.getDecoder().decode(encAesKeyBase64);

        //decrypt AES key with private RSA key
        byte[] decryptedAesKeyHex =
                CryptoUtil.decryptWithPrivateRsaKey(encAesKeyBytes, user.getRsaPrivateKey());


        byte[] decryptedAesKey = HexUtils.fromHexString(new String(decryptedAesKeyHex));

        //initialization vector - 1st 16 chars of userId
        byte []iv;
        if(user.getId().toString().length()>16)
            iv = user.getId().toString().substring(0,16).getBytes();
        else
            iv = (String.valueOf(user.getId())+"ABCDEFGHIJKLMNOP").substring(0,16).getBytes();

        byte[] encTransBytes = Base64.getDecoder().decode(encryptedTransaction.getPayload());

        //decrypt transaction payload with AES key
        byte[] decrypted = CryptoUtil.decryptWithAes(encTransBytes, decryptedAesKey, iv);

        System.out.println(new String(decrypted));
        //cast JSON string to Transaction object
        VoteRequest voteRequest = new Gson().fromJson(new String(decrypted), VoteRequest.class);

        //for example, call payment gateway with provided information

        Optional<Vote> vote = voteService.findByUserIdAndStatusAndElectionId(voteRequest.getUserId(), "ACT", voteRequest.getElectionId());

        if(vote.isPresent()){
            return ResponseEntity.ok(0);
        }
        else{
            Vote voteRegister = voteService.findByUserIdAndStatusAndElectionId(voteRequest.getUserId(), "PEN", voteRequest.getElectionId())
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra el voto del elector."));

            voteRegister.setVote(encoder.encode(voteRequest.getCandidateListId().toString()));

            voteRegister.setStatus("ACT");

            voteRegister.setUpdateDate(LocalDate.now());

            voteRegister.setUpdateUser(voteRegister.getCreationUser());

            voteService.saveAndFlush(voteRegister);

            return ResponseEntity.ok(1);
        }

    }
}
