package com.pucp.edu.pe.votingsystem.service;

import com.pucp.edu.pe.votingsystem.dao.BannerRepository;
import com.pucp.edu.pe.votingsystem.dto.Banner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class BannerService {
    @Autowired
    BannerRepository dao;

    public void saveAndFlush(Banner banner){dao.saveAndFlush(banner);}

    public Set<Banner> listAllActive(){ return dao.findAllByStatus("ACT");}
}
