package com.pucp.edu.pe.votingsystem.dto;

public enum ERole {
    ROLE_USER,
    ROLE_CANDIDATE,
    ROLE_ADMIN,
    ROLE_LIST_ADMIN
}
