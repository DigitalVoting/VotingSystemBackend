package com.pucp.edu.pe.votingsystem.service;

import com.pucp.edu.pe.votingsystem.dao.CandidatesListRepository;
import com.pucp.edu.pe.votingsystem.dto.CandidatesList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class CandidatesListService {
    @Autowired
    private CandidatesListRepository dao;

    public Optional<CandidatesList> findById(Integer id){ return dao.findById(id); }

    public void saveAndFlush(CandidatesList candidatesList){
        dao.saveAndFlush(candidatesList);
    }

    public Boolean existsByName(String name){
        return dao.existsByName(name);
    }

    public Set<CandidatesList> findAllByElection(Integer id){
        return dao.findByElectionsIdOrderByName(id);
    }

    public Set<CandidatesList> findByElectionsStatusAndStatus(String electionStatus, String listStatus ){
        return dao.findByElectionsStatusAndStatusOrderByName(electionStatus, listStatus);
    }

    public Optional<CandidatesList> findByName(String name){
        return dao.findByName(name);
    }

    public Set<CandidatesList> findByNameAndStatus(String name, String status){ return dao.findByNameContainingAndStatusOrderByName(name,status);}

    public Set<CandidatesList> findByNameAndNotStatus(String name, String status){ return dao.findByNameContainingAndStatusNotOrderByName(name,status);}
}
