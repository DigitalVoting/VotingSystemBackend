package com.pucp.edu.pe.votingsystem.dto;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(	name = "proposals")
public class Proposal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(max = 80)
    private String name;

    @NotBlank
    @Size(max = 500)
    private String description;

    @NotBlank
    @Size(max = 3)
    private String status;

    private Integer creation_user;

    private Integer update_user;

    private LocalDate creation_date;

    private LocalDate update_date;

    @OneToOne
    private CandidatesList candidatesList;

    public Proposal() {
    }

    public Proposal(String name, String description, String status, Integer creation_user) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.creation_user = creation_user;
        this.creation_date = LocalDate.now();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCreationUser() {
        return creation_user;
    }

    public void setCreationUser(Integer creation_user) {
        this.creation_user = creation_user;
    }

    public Integer getUpdateUser() {
        return update_user;
    }

    public void setUpdateUser(Integer update_user) {
        this.update_user = update_user;
    }

    public LocalDate getCreationDate() {
        return creation_date;
    }

    public void setCreationDate(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public LocalDate getUpdateDate() {
        return update_date;
    }

    public void setUpdateDate(LocalDate update_date) {
        this.update_date = update_date;
    }

    public CandidatesList getCandidatesList() {
        return candidatesList;
    }

    public void setCandidatesList(CandidatesList candidatesList) {
        this.candidatesList = candidatesList;
    }
}
