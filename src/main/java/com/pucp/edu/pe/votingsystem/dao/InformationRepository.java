package com.pucp.edu.pe.votingsystem.dao;

import com.pucp.edu.pe.votingsystem.dto.Information;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface InformationRepository extends JpaRepository<Information,Integer> {

    Set<Information> findByUserIdAndCandidatesListIdAndStatus(Integer userId, Integer candidateListId, String status);
}
