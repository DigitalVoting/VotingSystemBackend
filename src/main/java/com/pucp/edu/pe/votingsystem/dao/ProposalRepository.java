package com.pucp.edu.pe.votingsystem.dao;

import com.pucp.edu.pe.votingsystem.dto.Proposal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProposalRepository extends JpaRepository<Proposal, Integer> {
}
