package com.pucp.edu.pe.votingsystem.payload.request.Users;

import java.util.Set;

public class ChangeRolesRequest {
    private Integer updateUserId;

    private Integer userId;

    private Set<String> roles;

    public ChangeRolesRequest() {
    }

    public Integer getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Integer updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
