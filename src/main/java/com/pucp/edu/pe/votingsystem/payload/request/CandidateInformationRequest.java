package com.pucp.edu.pe.votingsystem.payload.request;

import java.text.ParseException;
import java.util.Set;

public class CandidateInformationRequest {

    private Integer userId;

    private Integer candidateListId;

    private Integer creationUserId;

    private Integer notificationId;

    private String status;

    public Integer getCreationUserId() {
        return creationUserId;
    }

    public void setCreationUserId(Integer creationUserId) {
        this.creationUserId = creationUserId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

    public static class CandidateInformation{

        private String role;

        private String enterprise;

        private String description;

        private String startDate;

        private String endDate;

        private String type;

        public CandidateInformation(){

        }

        public CandidateInformation(String role, String enterprise, String description, String startDate, String endDate, String type) throws ParseException {
            this.role = role;
            this.enterprise = enterprise;
            this.description = description;
            this.startDate = startDate; //2020-01-01
            this.endDate = endDate;
            this.type = type;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getEnterprise() {
            return enterprise;
        }

        public void setEnterprise(String enterprise) {
            this.enterprise = enterprise;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    private Set<CandidateInformation> candidateInformation;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCandidateListId() {
        return candidateListId;
    }

    public void setCandidateListId(Integer candidateListId) {
        this.candidateListId = candidateListId;
    }

    public Set<CandidateInformation> getCandidateInformation() {
        return candidateInformation;
    }

    public void setCandidateInformation(Set<CandidateInformation> candidateInformation) {
        this.candidateInformation = candidateInformation;
    }
}
