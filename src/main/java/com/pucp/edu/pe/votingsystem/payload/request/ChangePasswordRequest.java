package com.pucp.edu.pe.votingsystem.payload.request;

import javax.validation.constraints.NotBlank;

public class ChangePasswordRequest {

    private Integer userId;

    @NotBlank
    private String password;

    @NotBlank
    private String token;

    public ChangePasswordRequest() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
