package com.pucp.edu.pe.votingsystem.payload.request;

public class NotificationsRequest {
    private Integer userId;
    private Integer candidateListId;

    public NotificationsRequest() {
    }

    public NotificationsRequest(Integer userId, Integer candidateListId) {
        this.userId = userId;
        this.candidateListId = candidateListId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCandidateListId() {
        return candidateListId;
    }

    public void setCandidateListId(Integer candidateListId) {
        this.candidateListId = candidateListId;
    }
}
