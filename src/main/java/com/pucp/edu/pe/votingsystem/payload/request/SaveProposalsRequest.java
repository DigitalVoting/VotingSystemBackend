package com.pucp.edu.pe.votingsystem.payload.request;

import com.pucp.edu.pe.votingsystem.dto.Proposal;

import java.util.Set;

public class SaveProposalsRequest {

    private Integer candidateListId;

    private Integer creationUserId;

    private String status;

    private Set<Proposal> proposals;

    public SaveProposalsRequest() {
    }

    public Integer getCandidateListId() {
        return candidateListId;
    }

    public void setCandidateListId(Integer candidateListId) {
        this.candidateListId = candidateListId;
    }

    public Integer getCreationUserId() {
        return creationUserId;
    }

    public void setCreationUserId(Integer creationUserId) {
        this.creationUserId = creationUserId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<Proposal> getProposals() {
        return proposals;
    }

    public void setProposals(Set<Proposal> proposals) {
        this.proposals = proposals;
    }
}
