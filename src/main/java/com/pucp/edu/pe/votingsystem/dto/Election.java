package com.pucp.edu.pe.votingsystem.dto;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(	name = "elections",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "name"),
        })
public class Election {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(max = 120)
    private String name;

    @NotBlank
    @Size(max = 250)
    private String description;

    private LocalDateTime startDateInscriptions;

    private LocalDateTime endDateInscriptions;

    @NotBlank
    @Size(max = 250)
    private String descriptionInscriptions;

    private LocalDateTime startDateActualization;

    private LocalDateTime endDateActualization;

    @NotBlank
    @Size(max = 250)
    private String descriptionActualization;

    private LocalDateTime startDateVote;

    private LocalDateTime endDateVote;

    @NotBlank
    @Size(max = 250)
    private String descriptionVote;

    private String logo;

    @NotBlank
    @Size(max = 3)
    private String status;

    private Integer creationUser;

    private Integer updateUser;

    private LocalDate creationDate;

    private LocalDate updateDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinTable(	name = "election_candidate_list",
            joinColumns = @JoinColumn(name = "election_id"),
            inverseJoinColumns = @JoinColumn(name = "candidate_list_id"))
    private CandidatesList candidatesListWinner;

    public Election(){

    }

    public Election(String name, String description, String logo, LocalDateTime startDateInscriptions, LocalDateTime endDateInscriptions,
                    LocalDateTime startDateActualization, LocalDateTime endDateActualization, LocalDateTime startDateVote,
                    LocalDateTime endDateVote, String status, Integer creationUser, String descriptionInscriptions,
                    String descriptionActualization, String descriptionVote){
        this.name = name;
        this.description = description;
        this.logo = logo;
        this.startDateInscriptions = startDateInscriptions;
        this.endDateInscriptions = endDateInscriptions;
        this.startDateActualization = startDateActualization;
        this.endDateActualization = endDateActualization;
        this.startDateVote = startDateVote;
        this.endDateVote = endDateVote;
        this.descriptionInscriptions = descriptionInscriptions;
        this.descriptionActualization = descriptionActualization;
        this.descriptionVote = descriptionVote;
        this.status = status;
        this.creationUser = creationUser;
        this.creationDate = LocalDate.now();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(Integer creationUser) {
        this.creationUser = creationUser;
    }

    public Integer getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Integer update_user) {
        this.updateUser = updateUser;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate update_date) {
        this.updateDate = updateDate;
    }

    public CandidatesList getCandidatesListWinner() {
        return candidatesListWinner;
    }

    public void setCandidatesListWinner(CandidatesList candidatesList) {
        this.candidatesListWinner = candidatesList;
    }

    public LocalDateTime getStartDateInscriptions() {
        return startDateInscriptions;
    }

    public void setStartDateInscriptions(LocalDateTime startDateInscriptions) {
        this.startDateInscriptions = startDateInscriptions;
    }

    public LocalDateTime getEndDateInscriptions() {
        return endDateInscriptions;
    }

    public void setEndDateInscriptions(LocalDateTime endDateInscriptions) {
        this.endDateInscriptions = endDateInscriptions;
    }

    public LocalDateTime getStartDateActualization() {
        return startDateActualization;
    }

    public void setStartDateActualization(LocalDateTime startDateActualization) {
        this.startDateActualization = startDateActualization;
    }

    public LocalDateTime getEndDateActualization() {
        return endDateActualization;
    }

    public void setEndDateActualization(LocalDateTime endDateActualization) {
        this.endDateActualization = endDateActualization;
    }

    public LocalDateTime getStartDateVote() {
        return startDateVote;
    }

    public void setStartDateVote(LocalDateTime startDateVote) {
        this.startDateVote = startDateVote;
    }

    public LocalDateTime getEndDateVote() {
        return endDateVote;
    }

    public void setEndDateVote(LocalDateTime endDateVote) {
        this.endDateVote = endDateVote;
    }

    public String getDescriptionInscriptions() {
        return descriptionInscriptions;
    }

    public void setDescriptionInscriptions(String descriptionInscriptions) {
        this.descriptionInscriptions = descriptionInscriptions;
    }

    public String getDescriptionActualization() {
        return descriptionActualization;
    }

    public void setDescriptionActualization(String descriptionActualization) {
        this.descriptionActualization = descriptionActualization;
    }

    public String getDescriptionVote() {
        return descriptionVote;
    }

    public void setDescriptionVote(String descriptionVote) {
        this.descriptionVote = descriptionVote;
    }
}
