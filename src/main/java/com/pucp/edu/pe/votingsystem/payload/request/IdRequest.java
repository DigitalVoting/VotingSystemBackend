package com.pucp.edu.pe.votingsystem.payload.request;

public class IdRequest {
    private Integer id;

    public IdRequest(Integer id) {
        this.id = id;
    }

    public IdRequest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
