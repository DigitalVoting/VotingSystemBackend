package com.pucp.edu.pe.votingsystem.payload.response;

import java.time.LocalDate;

public class ActiveElection {
    private Integer id;
    private String title;
    private String banner;
    private String information;
    private String startDate;
    private String endDate;
    private String stage;

    public ActiveElection() {
    }

    public ActiveElection(Integer id, String title, String banner, String information, String startDate, String endDate, String stage) {
        this.id = id;
        this.title = title;
        this.banner = banner;
        this.information = information;
        this.startDate = startDate;
        this.endDate = endDate;
        this.stage = stage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }
}
