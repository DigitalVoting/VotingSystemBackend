package com.pucp.edu.pe.votingsystem.api;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

import javax.mail.*;
import javax.mail.internet.*;
import javax.validation.Valid;

import com.pucp.edu.pe.votingsystem.payload.request.ChangePasswordRequest;
import com.pucp.edu.pe.votingsystem.payload.request.LoginRequest;
import com.pucp.edu.pe.votingsystem.payload.request.SignupRequest;
import com.pucp.edu.pe.votingsystem.payload.response.JwtResponse;
import com.pucp.edu.pe.votingsystem.payload.response.MessageResponse;
import com.pucp.edu.pe.votingsystem.service.RoleService;
import com.pucp.edu.pe.votingsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pucp.edu.pe.votingsystem.dto.ERole;
import com.pucp.edu.pe.votingsystem.dto.Role;
import com.pucp.edu.pe.votingsystem.dto.User;
import com.pucp.edu.pe.votingsystem.security.Jwt.JwtUtils;
import com.pucp.edu.pe.votingsystem.security.Services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    private static final SecureRandom secureRandom = new SecureRandom(); //threadsafe
    private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder(); //threadsafe

    @Value("${spring.mail.username}")
    private String mailUsername;

    @Value("${spring.mail.password}")
    private String mailPassword;

    public void sendmail(String emailRecepient, String subject, String content) throws AddressException, MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailUsername, mailPassword);
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(mailUsername, false));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailRecepient));
        msg.setSubject(subject);
        msg.setContent(content, "text/html");
        msg.setSentDate(new Date());

        Transport.send(msg);
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
                        loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        Optional<User> user = userService.findById(userDetails.getId());

        return user.map(value -> ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                value.getNames(),
                value.getLastNames(),
                roles))).orElseGet(() -> ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                "",
                "",
                roles)));

    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userService.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Usuario ya existente!"));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                encoder.encode(signUpRequest.getPassword()),signUpRequest.getNames(),signUpRequest.getLastNames(),"ACT",signUpRequest.getCreationUser(), signUpRequest.getEmail());

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleService.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleService.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                        roles.add(adminRole);

                        break;
                    case "candidato":
                        Role modRole = roleService.findByName(ERole.ROLE_CANDIDATE)
                                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                        roles.add(modRole);

                        break;
                    case "representante":
                        Role repRole = roleService.findByName(ERole.ROLE_LIST_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                        roles.add(repRole);

                        break;
                    default:
                        Role userRole = roleService.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        userService.save(user);

        return ResponseEntity.ok(new MessageResponse("Usuario Registrado!"));
    }

    @PostMapping("/restore-password")
    public ResponseEntity<?> registerUser(@Valid @RequestBody LoginRequest loginRequest) {

        Optional<User> user = userService.findByUsername(loginRequest.getUsername());

        if(user.isPresent()){
            byte[] randomBytes = new byte[24];
            secureRandom.nextBytes(randomBytes);
            String token = base64Encoder.encodeToString(randomBytes);
            user.get().setToken(token);
            userService.save(user.get());
            try{
                sendmail(user.get().getEmail(),"Recuperación de Contraseña - TOKEN", "Su token es: "+token);
            }
            catch (Exception e){
                ResponseEntity.ok(e);
            }
            return ResponseEntity.ok(user.get().getId());
        }
        else{
            return ResponseEntity.ok(0);
        }

    }

    @PostMapping("/confirm-token")
    public ResponseEntity<?> confirmToken(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {

        Optional<User> user = userService.findById(changePasswordRequest.getUserId());

        if(user.isPresent()){
            if(user.get().getToken().equals(changePasswordRequest.getToken())){
                return ResponseEntity.ok(1);
            }
            else{
                return ResponseEntity.ok(0);
            }
        }
        else{
            return ResponseEntity.ok(-1);
        }

    }


    @PostMapping("/change-password")
    public ResponseEntity<?> changePassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {

        Optional<User> user = userService.findById(changePasswordRequest.getUserId());

        if(user.isPresent()){
            if (user.get().getToken()!=null){
                if(user.get().getToken().equals(changePasswordRequest.getToken())){
                    user.get().setPassword(encoder.encode(changePasswordRequest.getPassword()));
                    user.get().setToken(null);
                    userService.save(user.get());
                    return ResponseEntity.ok(1);
                }
                else{
                    return ResponseEntity.ok(0);
                }
            }
            else{
                return ResponseEntity.ok(0);
            }
        }
        else{
            return ResponseEntity.ok(-1);
        }

    }
}
