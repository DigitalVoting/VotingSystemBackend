package com.pucp.edu.pe.votingsystem.service;

import com.pucp.edu.pe.votingsystem.dao.RoleRepository;
import com.pucp.edu.pe.votingsystem.dto.ERole;
import com.pucp.edu.pe.votingsystem.dto.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {
    @Autowired
    private RoleRepository dao;

    public Optional<Role> findByName(ERole name){
        return dao.findByName(name);
    }
}
