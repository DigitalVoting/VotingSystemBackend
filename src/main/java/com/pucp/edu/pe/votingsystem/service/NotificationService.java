package com.pucp.edu.pe.votingsystem.service;

import com.pucp.edu.pe.votingsystem.dao.NotificationRepository;
import com.pucp.edu.pe.votingsystem.dto.Information;
import com.pucp.edu.pe.votingsystem.dto.Notification;
import jdk.nashorn.internal.ir.Optimistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class NotificationService {
    @Autowired
    NotificationRepository dao;

    public void saveAndFlush(Notification notification){
        dao.saveAndFlush(notification);
    }

    public Optional<Notification> findById(Integer id) { return dao.findById(id);}

    public Set<Notification> findByUserIdOrCandidatesListIdAndStatus(Integer userId, Integer candidatesListId, String status){ return dao.findByUserIdAndStatusOrCandidatesListIdAndStatusOrderByCreationDate(userId, status, candidatesListId, status);}

    public Set<Notification> findByStatus(String status){ return dao.findByStatus(status);}

    public Set<Notification> findByCandidatesListIdAndTitle(Integer candidatesListId, String title){ return dao.findByCandidatesListIdAndTitle(candidatesListId, title);}
}
