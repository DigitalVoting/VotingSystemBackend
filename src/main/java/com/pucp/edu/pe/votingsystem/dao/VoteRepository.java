package com.pucp.edu.pe.votingsystem.dao;

import com.pucp.edu.pe.votingsystem.dto.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface VoteRepository extends JpaRepository<Vote, Integer> {

    Optional<Vote> findByUserIdAndStatusAndElectionId(Integer userId, String status, Integer electionId);

    Set<Vote> findByElectionIdAndStatus(Integer electionId, String status);

}
