package com.pucp.edu.pe.votingsystem.payload.request;

public class FindRequest {

    private String name;

    public FindRequest() {
    }

    public FindRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
