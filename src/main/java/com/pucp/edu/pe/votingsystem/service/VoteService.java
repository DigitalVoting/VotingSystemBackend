package com.pucp.edu.pe.votingsystem.service;

import com.pucp.edu.pe.votingsystem.dao.VoteRepository;
import com.pucp.edu.pe.votingsystem.dto.Vote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class VoteService {
    @Autowired
    VoteRepository dao;

    public Optional<Vote> findByUserIdAndStatusAndElectionId(Integer userId, String status, Integer electionId){
        return dao.findByUserIdAndStatusAndElectionId(userId, status, electionId);
    }

    public Set<Vote> findByElectionIdAndStatus(Integer electionId, String status){
        return dao.findByElectionIdAndStatus(electionId, status);
    }

    public void saveAndFlush(Vote vote){ dao.saveAndFlush(vote);}
}
