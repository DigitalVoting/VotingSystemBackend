package com.pucp.edu.pe.votingsystem.dao;

import com.pucp.edu.pe.votingsystem.dto.Election;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

public interface ElectionRepository extends JpaRepository<Election, Integer> {
    ArrayList<Election> findByStatus(String status);

    Set<Election> findByStatusNot(String status);

    Set<Election> findByNameContainingAndStatusNot(String name, String status);

    Optional<Election> findByName(String name);
}
