package com.pucp.edu.pe.votingsystem.api;

import com.pucp.edu.pe.votingsystem.dto.CandidatesList;
import com.pucp.edu.pe.votingsystem.dto.ERole;
import com.pucp.edu.pe.votingsystem.dto.Role;
import com.pucp.edu.pe.votingsystem.dto.User;
import com.pucp.edu.pe.votingsystem.payload.request.CandidateInformationRequest;
import com.pucp.edu.pe.votingsystem.payload.request.FindRequest;
import com.pucp.edu.pe.votingsystem.payload.request.IdRequest;
import com.pucp.edu.pe.votingsystem.payload.request.SignupRequest;
import com.pucp.edu.pe.votingsystem.payload.request.Users.ChangeRolesRequest;
import com.pucp.edu.pe.votingsystem.payload.request.Users.RegisterListRequest;
import com.pucp.edu.pe.votingsystem.payload.response.MessageResponse;
import com.pucp.edu.pe.votingsystem.payload.response.Users.RegisterListResponse;
import com.pucp.edu.pe.votingsystem.service.CandidatesListService;
import com.pucp.edu.pe.votingsystem.service.RoleService;
import com.pucp.edu.pe.votingsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
public class UserApi {
    @Autowired
    private UserService userService;

    @Autowired
    private CandidatesListService candidatesListService;

    @Autowired
    private RoleService roleService;

    @Autowired
    PasswordEncoder encoder;

    @Value("${spring.mail.username}")
    private String mailUsername;

    @Value("${spring.mail.password}")
    private String mailPassword;

    public void sendmail(String emailRecepient, String subject, String content) throws AddressException, MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailUsername, mailPassword);
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(mailUsername, false));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailRecepient));
        msg.setSubject(subject);
        msg.setContent(content, "text/html");
        msg.setSentDate(new Date());

        Transport.send(msg);
    }

    @PostMapping("/candidates/listcurrent")
    public Set<User> listAllCurrentLists(){
        return userService.findAllCurrentCandidates("ACT");
    }

    @PostMapping("/candidates/find")
    public ResponseEntity<?> findCandidatesLists(@Valid @RequestBody FindRequest findRequest){
        return ResponseEntity.ok(userService.findByName(findRequest.getName(),findRequest.getName()));
    }

    @PostMapping("/get")
    public ResponseEntity<?> getUser(@Valid @RequestBody CandidateInformationRequest candidateInformationRequest){
        return ResponseEntity.ok(userService.findById(candidateInformationRequest.getUserId()));
    }

    @PostMapping("/get-current-list")
    public ResponseEntity<?> getCurrentList(@Valid @RequestBody IdRequest idRequest){
        User candidate = userService.findById(idRequest.getId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Usuario."));

        for (CandidatesList candidatesList :
                candidate.getCandidatesLists()) {
            if(candidatesList.getStatus().equals("ACT") || candidatesList.getStatus().equals("PEN") || candidatesList.getStatus().equals("SOL")){
                return ResponseEntity.ok(candidatesList);
            }
        }

        return ResponseEntity.ok(new MessageResponse("No cuenta con listas activas"));
    }

    @PostMapping("/get-active")
    public ResponseEntity<?> getActiveUsers(){
        return ResponseEntity.ok(userService.findByNameOrLastNamesAndStatusAndNotRole("","","ACT", ERole.ROLE_ADMIN));
    }

    @PostMapping("/inactive")
    public ResponseEntity<?> getInactiveUsers(@Valid @RequestBody FindRequest findRequest){
        return ResponseEntity.ok(userService.findByNameOrLastNamesAndNotStatusAndNotRole(findRequest.getName(),findRequest.getName(),"ACT", ERole.ROLE_ADMIN));
    }

    @PostMapping("/find-active")
    public ResponseEntity<?> findActiveUsers(@Valid @RequestBody FindRequest findRequest){
        return ResponseEntity.ok(userService.findByNameOrLastNamesAndStatusAndNotRole(findRequest.getName(),findRequest.getName(),"ACT", ERole.ROLE_ADMIN));
    }

    @PostMapping("/find-active-electors")
    public ResponseEntity<?> findActiveElectors(@Valid @RequestBody FindRequest findRequest){
        return ResponseEntity.ok(userService.findByNameOrLastNamesAndStatusAndRole(findRequest.getName(),findRequest.getName(),"ACT", ERole.ROLE_USER));
    }

    @PostMapping("/change-roles")
    public ResponseEntity<?> changeUserRoles(@Valid @RequestBody ChangeRolesRequest changeRolesRequest){

        User user = userService.findById(changeRolesRequest.getUserId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Usuario."));

        Set<Role> roles = new HashSet<>();

        for (String role :
                changeRolesRequest.getRoles()) {
            switch (role) {
                case "admin":
                    Role adminRole = roleService.findByName(ERole.ROLE_ADMIN)
                            .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                    roles.add(adminRole);

                    break;
                case "candidato":
                    Role modRole = roleService.findByName(ERole.ROLE_CANDIDATE)
                            .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                    roles.add(modRole);

                    break;
                case "representante":
                    Role repRole = roleService.findByName(ERole.ROLE_LIST_ADMIN)
                            .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                    roles.add(repRole);

                    break;
                default:
                    Role userRole = roleService.findByName(ERole.ROLE_USER)
                            .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                    roles.add(userRole);
            }

        }

        user.setRoles(roles);
        user.setUpdateUser(changeRolesRequest.getUpdateUserId());
        user.setUpdateDate(LocalDate.now());

        userService.save(user);

        return ResponseEntity.ok(new MessageResponse("Se actualizaron los roles exitosamente"));
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userService.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Usuario ya existente!"));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                encoder.encode(signUpRequest.getPassword()),signUpRequest.getNames(),signUpRequest.getLastNames(),"ACT",signUpRequest.getCreationUser(), signUpRequest.getEmail());

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleService.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleService.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                        roles.add(adminRole);

                        break;
                    case "candidato":
                        Role modRole = roleService.findByName(ERole.ROLE_CANDIDATE)
                                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                        roles.add(modRole);

                        break;
                    case "representante":
                        Role repRole = roleService.findByName(ERole.ROLE_LIST_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                        roles.add(repRole);

                        break;
                    default:
                        Role userRole = roleService.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        userService.save(user);

        try{
            sendmail(user.getEmail(),"Cuenta Registrada",
                    "Puede visitar el sistema de votación ingresando con su usuario: " + signUpRequest.getUsername()
                            + " y contraseña: " + signUpRequest.getPassword());
        }
        catch (Exception e){
            User userException = userService.findById(0)
                    .orElseThrow(() -> new RuntimeException("Error: No se pudo realizar el envío del mensaje al email."));
        }

        return ResponseEntity.ok(new MessageResponse("Usuario Registrado!"));
    }

    @PostMapping("/register-list")
    public ResponseEntity<?> registerUsersList(@Valid @RequestBody RegisterListRequest registerListRequest) {

        String strRole = registerListRequest.getRole();

        Set<Role> roles = new HashSet<>();

        if (strRole == null) {
            User user = userService.findById(0)
                    .orElseThrow(() -> new RuntimeException("Error: Es necesario definir el Rol."));
        }
        else{
            switch (strRole) {
                case "admin":
                    Role adminRole = roleService.findByName(ERole.ROLE_ADMIN)
                            .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                    roles.add(adminRole);

                    break;
                case "candidato":
                    Role modRole = roleService.findByName(ERole.ROLE_CANDIDATE)
                            .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                    roles.add(modRole);

                    break;
                case "representante":
                    Role repRole = roleService.findByName(ERole.ROLE_LIST_ADMIN)
                            .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                    roles.add(repRole);

                    break;
                default:
                    Role userRole = roleService.findByName(ERole.ROLE_USER)
                            .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                    roles.add(userRole);
            }
        }

        Set<User> usersRegistered = new HashSet<>();
        Set<RegisterListRequest.User> usersNotRegistered = new HashSet<>();

        for (RegisterListRequest.User user:
             registerListRequest.getUsers()) {
            if (userService.existsByUsername(user.getUsername())) {
                usersNotRegistered.add(user);
            }
            else{
                String password = user.getUsername()+"123";
                // Create new user's account
                User newUser = new User(user.getUsername(),
                        encoder.encode(password),user.getNames(),user.getLastNames(),"ACT",registerListRequest.getCreationUser(), user.getEmail());
                newUser.setRoles(roles);

                usersRegistered.add(newUser);

                try{
                    sendmail(user.getEmail(),"Cuenta Registrada",
                            "Puede visitar el sistema de votación ingresando con su usuario: " + user.getUsername()
                                    + " y contraseña: " + password);
                }
                catch (Exception e){
                    User userException = userService.findById(0)
                            .orElseThrow(() -> new RuntimeException("Error: No se pudo realizar el envío del mensaje al email."));
                }
            }
        }

        userService.saveUsers(usersRegistered);

        return ResponseEntity.ok(new RegisterListResponse(usersRegistered, usersNotRegistered));
    }

}
