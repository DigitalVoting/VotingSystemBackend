package com.pucp.edu.pe.votingsystem.payload.request.Elections;

public class ConfirmTokenRequest {
    private Integer userId;

    private Integer electionId;

    private String token;

    public ConfirmTokenRequest() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getElectionId() {
        return electionId;
    }

    public void setElectionId(Integer electionId) {
        this.electionId = electionId;
    }
}
