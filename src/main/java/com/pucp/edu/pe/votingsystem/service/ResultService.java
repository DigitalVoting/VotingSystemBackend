package com.pucp.edu.pe.votingsystem.service;

import com.pucp.edu.pe.votingsystem.dao.ResultRepository;
import com.pucp.edu.pe.votingsystem.dto.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class ResultService {
    @Autowired
    private ResultRepository dao;

    public Optional<Result> findById(Integer id){
        return dao.findById(id);
    }

    public void saveResults(Set<Result> results){ dao.saveAll(results); }

    public void saveAndFlush(Result result){ dao.saveAndFlush(result); }

    public Set<Result> findByElectionIdOrderByNumberVotes(Integer electionId){ return dao.findByElectionIdOrderByNumberVotesDesc(electionId); }

}
