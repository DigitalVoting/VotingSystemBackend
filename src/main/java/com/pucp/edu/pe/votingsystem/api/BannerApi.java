package com.pucp.edu.pe.votingsystem.api;

import com.pucp.edu.pe.votingsystem.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/banner")
public class BannerApi {
    @Autowired
    BannerService bannerService;

    @PostMapping("/list-active")
    public ResponseEntity<?> listAllActiveBanners(){
        return ResponseEntity.ok(bannerService.listAllActive());
    }
}
