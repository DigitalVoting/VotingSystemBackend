package com.pucp.edu.pe.votingsystem.dao;

import com.pucp.edu.pe.votingsystem.dto.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface NotificationRepository extends JpaRepository<Notification, Integer> {
    Set<Notification> findByUserIdAndStatusOrCandidatesListIdAndStatusOrderByCreationDate(Integer userId, String status1, Integer candidateListId, String status2);

    Set<Notification> findByStatus(String status);

    Set<Notification> findByCandidatesListIdAndTitle(Integer candidatesListId, String title);
}
