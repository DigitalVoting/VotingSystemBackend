package com.pucp.edu.pe.votingsystem.dao;

import com.pucp.edu.pe.votingsystem.dto.ERole;
import com.pucp.edu.pe.votingsystem.dto.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role,Integer> {

    Optional<Role> findByName(ERole name);
}
