package com.pucp.edu.pe.votingsystem.dto;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ERole name;

    private Integer creation_user;

    private Integer update_user;

    private LocalDate creation_date;

    private LocalDate update_date;

    public Role() {

    }

    public Role(ERole name, Integer creation_user) {
        this.name = name;
        this.creation_user = creation_user;
        this.creation_date = LocalDate.now();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ERole getName() {
        return name;
    }

    public void setName(ERole name) {
        this.name = name;
    }

    public Integer getCreationUser() {
        return creation_user;
    }

    public void setCreationUser(Integer creation_user) {
        this.creation_user = creation_user;
    }

    public Integer getUpdateUser() {
        return update_user;
    }

    public void setUpdateUser(Integer update_user) {
        this.update_user = update_user;
    }

    public LocalDate getCreationDate() {
        return creation_date;
    }

    public void setCreationDate(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public LocalDate getUpdateDate() {
        return update_date;
    }

    public void setUpdateDate(LocalDate update_date) {
        this.update_date = update_date;
    }
}