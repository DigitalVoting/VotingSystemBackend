package com.pucp.edu.pe.votingsystem.api;

import com.pucp.edu.pe.votingsystem.dto.ERole;
import com.pucp.edu.pe.votingsystem.dto.Election;
import com.pucp.edu.pe.votingsystem.dto.Vote;
import com.pucp.edu.pe.votingsystem.payload.response.MessageResponse;
import com.pucp.edu.pe.votingsystem.service.ElectionService;
import com.pucp.edu.pe.votingsystem.service.UserService;
import com.pucp.edu.pe.votingsystem.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/dashboard")
public class DashboardApi {
    @Autowired
    VoteService voteService;

    @Autowired
    ElectionService electionService;

    @Autowired
    UserService userService;

    private class Result {
        private String label;
        private Double result;

        public void Results() {
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Double getResult() {
            return result;
        }

        public void setResult(Double result) {
            this.result = result;
        }
    }

    @PostMapping("/diary-votes")
    public ResponseEntity<?> getDiaryVotes(){

        ArrayList<Election> electionOpt = electionService.findActiveElection();

        if(electionOpt.get(0) != null){
            Election election = electionOpt.get(0);

            Set<Vote> votes = voteService.findByElectionIdAndStatus(election.getId(), "ACT");
            Map<LocalDate, Integer> diaryVotesMap = new HashMap<>();

            for (Vote vote :
                    votes) {
                if(diaryVotesMap.containsKey(vote.getUpdateDate())){
                    Integer numberVotes = diaryVotesMap.get(vote.getUpdateDate());

                    numberVotes = numberVotes + 1;

                    diaryVotesMap.remove(vote.getUpdateDate());

                    diaryVotesMap.put(vote.getUpdateDate(), numberVotes);

                }
                else {
                    diaryVotesMap.put(vote.getUpdateDate(), 1);
                }
            }

            List<Result> results = new ArrayList<>();

            for (Map.Entry<LocalDate, Integer> day:
                    diaryVotesMap.entrySet()){
                Result result = new Result();

                result.setResult(day.getValue()/1.0);

                result.setLabel(day.getKey().toString());

                results.add(result);

            }

            return ResponseEntity.ok(results);

        }
        else{
            return ResponseEntity.badRequest().body(new MessageResponse("No se encuentra alguna elección activa"));
        }
    }

    @PostMapping("/diary-electoral-participation")
    public ResponseEntity<?> getDiaryElectoralParticipation(){

        ArrayList<Election> electionOpt = electionService.findActiveElection();

        if(electionOpt.get(0) != null){
            Election election = electionOpt.get(0);

            Set<Vote> votes = voteService.findByElectionIdAndStatus(election.getId(), "ACT");
            Integer numberElectors = userService.findByNameOrLastNamesAndStatusAndNotRole("","","ACT", ERole.ROLE_ADMIN).size();

            Map<LocalDate, Integer> diaryVotesMap = new HashMap<>();

            for (Vote vote :
                    votes) {
                if(diaryVotesMap.containsKey(vote.getUpdateDate())){
                    Integer numberVotes = diaryVotesMap.get(vote.getUpdateDate());

                    numberVotes = numberVotes + 1;

                    diaryVotesMap.remove(vote.getUpdateDate());

                    diaryVotesMap.put(vote.getUpdateDate(), numberVotes);

                }
                else {
                    diaryVotesMap.put(vote.getUpdateDate(), 1);
                }
            }

            List<Result> results = new ArrayList<>();

            for (Map.Entry<LocalDate, Integer> day:
                    diaryVotesMap.entrySet()){
                Result result = new Result();

                result.setLabel(day.getKey().toString());

                result.setResult((Double.valueOf(day.getValue()))/numberElectors);

                results.add(result);
            }

            return ResponseEntity.ok(results);

        }
        else{
            return ResponseEntity.badRequest().body(new MessageResponse("No se encuentra alguna elección activa"));
        }
    }

    @PostMapping("/electoral-participation")
    public ResponseEntity<?> getElectoralParticipation(){

        ArrayList<Election> electionOpt = electionService.findActiveElection();

        if(electionOpt.get(0) != null) {
            Election election = electionOpt.get(0);

            Set<Vote> votes = voteService.findByElectionIdAndStatus(election.getId(), "ACT");
            Integer numberElectors = userService.findByNameOrLastNamesAndStatusAndNotRole("","","ACT", ERole.ROLE_ADMIN).size();

            return ResponseEntity.ok(Double.valueOf(votes.size())/numberElectors);

        }
        else{
            return ResponseEntity.badRequest().body(new MessageResponse("No se encuentra alguna elección activa"));
        }
    }

    @PostMapping("/total-electors")
    public ResponseEntity<?> getTotalElectors(){

        Integer numberElectors = userService.findByNameOrLastNamesAndStatusAndNotRole("","","ACT", ERole.ROLE_ADMIN).size();

        return ResponseEntity.ok(numberElectors);

    }

}
