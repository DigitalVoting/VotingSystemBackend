package com.pucp.edu.pe.votingsystem.service;

import com.pucp.edu.pe.votingsystem.dao.ElectionRepository;
import com.pucp.edu.pe.votingsystem.dto.Election;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

@Service
public class ElectionService {
    @Autowired
    private ElectionRepository dao;

    public Election saveAndFlush(Election election){
        return dao.saveAndFlush(election);
    }

    public Optional<Election> findById(Integer id){
        return dao.findById(id);
    }

    public ArrayList<Election> findActiveElection(){ return dao.findByStatus("ACT");}

    public Set<Election> findbyNotStatus(String status){ return dao.findByStatusNot(status);}

    public Set<Election> findbyNameAndNotStatus(String name, String status){ return dao.findByNameContainingAndStatusNot(name, status);}

    public Optional<Election> findbyName(String name){ return dao.findByName(name);}


}
