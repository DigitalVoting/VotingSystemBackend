package com.pucp.edu.pe.votingsystem.dto;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(	name = "information")
public class Information {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(max = 80)
    private String role;

    @NotBlank
    @Size(max = 80)
    private String enterprise;

    private String description;

    private LocalDate start_date;

    private LocalDate end_date;

    @NotBlank
    private String type;

    @NotBlank
    @Size(max = 3)
    private String status;

    private Integer creation_user;

    private Integer update_user;

    private LocalDate creation_date;

    private LocalDate update_date;

    @ManyToOne
    @JoinTable(	name = "information_users",
            joinColumns = @JoinColumn(name = "information_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private User user;

    @OneToOne
    @JoinTable(	name = "information_candidatesList",
            joinColumns = @JoinColumn(name = "information_id"),
            inverseJoinColumns = @JoinColumn(name = "candidatesList_id"))
    private CandidatesList candidatesList;


    public Information(){

    }

    public Information(String role, String enterprise,
                       String description, LocalDate start_date, LocalDate end_date, String type, String status, Integer creation_user) {
        this.role = role;
        this.enterprise = enterprise;
        this.description = description;
        this.start_date = start_date;
        this.end_date = end_date;
        this.type = type;
        this.setStatus(status);
        this.creation_user = creation_user;
        this.creation_date = LocalDate.now();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartDate() {
        return start_date;
    }

    public void setStartDate(LocalDate start_date) {
        this.start_date = start_date;
    }

    public LocalDate getEndDate() {
        return end_date;
    }

    public void setEndDate(LocalDate end_date) {
        this.end_date = end_date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CandidatesList getCandidatesList() {
        return candidatesList;
    }

    public void setCandidatesList(CandidatesList candidatesList) {
        this.candidatesList = candidatesList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCreationUser() {
        return creation_user;
    }

    public void setCreationUser(Integer creation_user) {
        this.creation_user = creation_user;
    }

    public Integer getUpdateUser() {
        return update_user;
    }

    public void setUpdateUser(Integer update_user) {
        this.update_user = update_user;
    }

    public LocalDate getCreationDate() {
        return creation_date;
    }

    public void setCreationDate(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public LocalDate getUpdateDate() {
        return update_date;
    }

    public void setUpdateDate(LocalDate update_date) {
        this.update_date = update_date;
    }
}
