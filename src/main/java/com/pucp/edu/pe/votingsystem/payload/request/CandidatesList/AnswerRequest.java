package com.pucp.edu.pe.votingsystem.payload.request.CandidatesList;

public class AnswerRequest {
    private Integer userId;

    private Integer candidatesListId;

    private Boolean answer;

    private Integer notificationId;

    public AnswerRequest() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCandidatesListId() {
        return candidatesListId;
    }

    public void setCandidatesListId(Integer candidatesListId) {
        this.candidatesListId = candidatesListId;
    }

    public Boolean getAnswer() {
        return answer;
    }

    public void setAnswer(Boolean answer) {
        this.answer = answer;
    }

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }
}
