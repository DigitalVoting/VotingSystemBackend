package com.pucp.edu.pe.votingsystem.dao;

import com.pucp.edu.pe.votingsystem.dto.CandidatesList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface CandidatesListRepository extends JpaRepository<CandidatesList,Integer> {
    Boolean existsByName(String name);

    Set<CandidatesList> findByElectionsIdOrderByName(Integer id);

    Set<CandidatesList> findByElectionsStatusAndStatusOrderByName(String status1, String status2);

    Optional<CandidatesList> findByName(String name);

    Set<CandidatesList> findByNameContainingAndStatusOrderByName(String name, String status);

    Set<CandidatesList> findByNameContainingAndStatusNotOrderByName(String name, String status);
}
