package com.pucp.edu.pe.votingsystem.payload.response;

import java.util.Set;

public class CandidatesSolicitudesResponse {
    private String status;

    private Set<Integer> rejectedCandidates;

    private Set<Integer> acceptedCandidates;

    private Set<Integer> pendingCandidates;

    public CandidatesSolicitudesResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<Integer> getRejectedCandidates() {
        return rejectedCandidates;
    }

    public void setRejectedCandidates(Set<Integer> rejectedCandidates) {
        this.rejectedCandidates = rejectedCandidates;
    }

    public Set<Integer> getAcceptedCandidates() {
        return acceptedCandidates;
    }

    public void setAcceptedCandidates(Set<Integer> acceptedCandidates) {
        this.acceptedCandidates = acceptedCandidates;
    }

    public Set<Integer> getPendingCandidates() {
        return pendingCandidates;
    }

    public void setPendingCandidates(Set<Integer> pendingCandidates) {
        this.pendingCandidates = pendingCandidates;
    }
}
