package com.pucp.edu.pe.votingsystem.api;

import com.pucp.edu.pe.votingsystem.dto.CandidatesList;
import com.pucp.edu.pe.votingsystem.dto.Information;
import com.pucp.edu.pe.votingsystem.dto.Notification;
import com.pucp.edu.pe.votingsystem.dto.User;
import com.pucp.edu.pe.votingsystem.payload.request.CandidateInformationRequest;
import com.pucp.edu.pe.votingsystem.payload.response.MessageResponse;
import com.pucp.edu.pe.votingsystem.service.CandidatesListService;
import com.pucp.edu.pe.votingsystem.service.InformationService;
import com.pucp.edu.pe.votingsystem.service.NotificationService;
import com.pucp.edu.pe.votingsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/information")
public class InformationApi {
    @Autowired
    InformationService informationService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    UserService userService;

    @Autowired
    CandidatesListService candidatesListService;

    @PostMapping("/candidate")
    public Set<Information> getCandidateInformation(@Valid @RequestBody CandidateInformationRequest candidateInformationRequest){
        if (candidateInformationRequest.getStatus().equals("profile")){
            Set<Information> actInformation = informationService.findByUserIdAndCandidatesListId(candidateInformationRequest.getUserId(),candidateInformationRequest.getCandidateListId(),"SOL");
            if(actInformation.isEmpty()){
                return informationService.findByUserIdAndCandidatesListId(candidateInformationRequest.getUserId(),candidateInformationRequest.getCandidateListId(),"ACT");
            }
            return actInformation;
        }
        else if (candidateInformationRequest.getStatus().equals("sol")){
            return informationService.findByUserIdAndCandidatesListId(candidateInformationRequest.getUserId(),candidateInformationRequest.getCandidateListId(),"SOL");
        }
        else{
            return informationService.findByUserIdAndCandidatesListId(candidateInformationRequest.getUserId(),candidateInformationRequest.getCandidateListId(),"ACT");
        }
    }

    @PostMapping("/saveInformation")
    public ResponseEntity<?> saveCandidateInformation(@Valid @RequestBody CandidateInformationRequest candidateInformationRequest){

        User candidate = userService.findById(candidateInformationRequest.getUserId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Usuario."));

        CandidatesList candidateList = candidatesListService.findById(candidateInformationRequest.getCandidateListId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha Lista de Candidatos."));

        for (CandidateInformationRequest.CandidateInformation information:
             candidateInformationRequest.getCandidateInformation()) {

            Information candidateInformation = new Information();

            if((information.getStartDate() == null  || information.getStartDate().equals("") ) && (information.getEndDate() == null  || information.getEndDate().equals(""))){
                candidateInformation.setRole("none");
                candidateInformation.setEnterprise("none");
                candidateInformation.setDescription(information.getDescription());
                candidateInformation.setType(information.getType());
                candidateInformation.setStatus(candidateInformationRequest.getStatus());
            }
            else{
                if(information.getEndDate() == null || information.getEndDate().equals("")) {
                    information.setEndDate(LocalDate.now().toString());
                }

                candidateInformation = new Information(information.getRole(),information.getEnterprise(),information.getDescription(),
                        LocalDate.parse(information.getStartDate()),LocalDate.parse(information.getEndDate()), information.getType(),
                        candidateInformationRequest.getStatus(), candidateInformationRequest.getCreationUserId());

                if(information.getEndDate() == null || information.getEndDate().equals("")) {
                    candidateInformation.setEndDate(null);
                }
            }

            candidateInformation.setUser(candidate);
            candidateInformation.setCandidatesList(candidateList);
            candidateInformation.setCreationDate(LocalDate.now());
            candidateInformation.setCreationUser(candidateInformationRequest.getCreationUserId());

            informationService.saveAndFlush(candidateInformation);
        }

        Notification notification = new Notification();

        notification.setTitle("Actualización de Información");
        notification.setDescription(candidate.getNames()+" "+candidate.getLastNames()+" solicita la actualización de sus datos a los presentados en el detalle de esta solicitud");
        notification.setCreationUser(candidateInformationRequest.getUserId());
        notification.setCreationDate(LocalDateTime.now());
        notification.setStatus("SOL");
        notification.setUser(candidate);
        notification.setCandidatesList(candidateList);

        notificationService.saveAndFlush(notification);

        return ResponseEntity.ok(new MessageResponse("Información Registrada Exitosamente!"));
    }

    @PostMapping("/accept-changes")
    public ResponseEntity<?> acceptChanges(@Valid @RequestBody CandidateInformationRequest candidateInformationRequest){

        Set<Information> informationAct = informationService.findByUserIdAndCandidatesListId(candidateInformationRequest.getUserId(),candidateInformationRequest.getCandidateListId(),"ACT");
        Set<Information> informationSol = informationService.findByUserIdAndCandidatesListId(candidateInformationRequest.getUserId(),candidateInformationRequest.getCandidateListId(),"SOL");

        Notification notificationSol = notificationService.findById(candidateInformationRequest.getNotificationId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha solicitud."));

        notificationSol.setStatus("DEL");
        notificationService.saveAndFlush(notificationSol);


        if(candidateInformationRequest.getStatus().equals("ACT")){
            for (Information information :
                    informationAct) {
                information.setStatus("DEL");
                information.setUpdateDate(LocalDate.now());
                information.setUpdateUser(candidateInformationRequest.getCreationUserId());

                informationService.saveAndFlush(information);
            }

            for (Information information:
                    informationSol) {
                information.setStatus("ACT");
                information.setUpdateDate(LocalDate.now());
                information.setUpdateUser(candidateInformationRequest.getCreationUserId());

                informationService.saveAndFlush(information);
            }

            Notification notification = new Notification();

            User user = userService.findById(candidateInformationRequest.getUserId())
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Usuario."));
            CandidatesList candidatesList = candidatesListService.findById(candidateInformationRequest.getCandidateListId())
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha Lista de Candidatos."));

            notification.setTitle("Actualización de Información");
            notification.setDescription(user.getNames()+" "+user.getLastNames()+" su solicitud para la actualización de sus datos ha sido aceptada");
            notification.setCreationUser(candidateInformationRequest.getCreationUserId());
            notification.setCreationDate(LocalDateTime.now());
            notification.setStatus("ACT");
            notification.setUser(user);
            notification.setCandidatesList(candidatesList);

            notificationService.saveAndFlush(notification);
        }
        else{
            for (Information information:
                    informationSol) {
                information.setStatus("REC");
                information.setUpdateDate(LocalDate.now());
                information.setUpdateUser(candidateInformationRequest.getCreationUserId());

                informationService.saveAndFlush(information);
            }

            Notification notification = new Notification();

            User user = userService.findById(candidateInformationRequest.getUserId())
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Usuario."));
            CandidatesList candidatesList = candidatesListService.findById(candidateInformationRequest.getCandidateListId())
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha Lista de Candidatos."));

            notification.setTitle("Actualización de Información");
            notification.setDescription(user.getNames()+" "+user.getLastNames()+" su solicitud para la actualización de sus datos ha sido rechazada");
            notification.setCreationUser(candidateInformationRequest.getCreationUserId());
            notification.setCreationDate(LocalDateTime.now());
            notification.setStatus("ACT");
            notification.setUser(user);
            notification.setCandidatesList(candidatesList);

            notificationService.saveAndFlush(notification);
        }


        return ResponseEntity.ok("Actualización Registrada Exitosamente");
    }
}
