package com.pucp.edu.pe.votingsystem.payload.response.Users;

import com.pucp.edu.pe.votingsystem.dto.User;
import com.pucp.edu.pe.votingsystem.payload.request.Users.RegisterListRequest;

import java.util.Set;

public class RegisterListResponse {
    private Set<User> usersRegistered;
    private Set<RegisterListRequest.User> usersNotRegistered;

    public RegisterListResponse() {
    }

    public RegisterListResponse(Set<User> usersRegistered, Set<RegisterListRequest.User> usersNotRegistered) {
        this.usersRegistered = usersRegistered;
        this.usersNotRegistered = usersNotRegistered;
    }

    public Set<User> getUsersRegistered() {
        return usersRegistered;
    }

    public void setUsersRegistered(Set<User> usersRegistered) {
        this.usersRegistered = usersRegistered;
    }

    public Set<RegisterListRequest.User> getUsersNotRegistered() {
        return usersNotRegistered;
    }

    public void setUsersNotRegistered(Set<RegisterListRequest.User> usersNotRegistered) {
        this.usersNotRegistered = usersNotRegistered;
    }
}
