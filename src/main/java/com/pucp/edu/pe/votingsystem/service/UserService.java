package com.pucp.edu.pe.votingsystem.service;

import com.pucp.edu.pe.votingsystem.dao.UserRepository;
import com.pucp.edu.pe.votingsystem.dto.ERole;
import com.pucp.edu.pe.votingsystem.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {
    @Autowired
    private UserRepository dao;

    public Boolean existsByUsername(String username){
        return dao.existsByUsername(username);
    }

    public void save(User user){
        dao.saveAndFlush(user);
    }

    public void saveUsers(Set<User> users){
        dao.saveAll(users);
    }

    public Optional<User> findById(Integer id){
        return dao.findById(id);
    }

    public List<User> findAll(){ return dao.findAll();}

    public Set<User> findByStatus(String status){ return dao.findByStatus(status);}

    public Set<User> findByNameOrLastNamesAndStatus(String names, String lastNames, String status){ return dao.findByNamesContainingAndStatusOrLastNamesContainingAndStatusOrderByNamesAscLastNamesAsc(names,status,lastNames,status);}

    public Set<User> findByNameOrLastNamesAndStatusAndRole(String names, String lastNames, String status, ERole role){
        return dao.findByNamesContainingAndStatusAndRolesNameOrLastNamesContainingAndStatusAndRolesNameOrderByNamesAscLastNamesAsc(
                names, status, role, lastNames, status, role
        );
    }

    public Set<User> findByNameOrLastNamesAndStatusAndNotRole(String names, String lastNames, String status, ERole role){
        return dao.findByNamesContainingAndStatusAndRolesNameNotOrLastNamesContainingAndStatusAndRolesNameNotOrderByNamesAscLastNamesAsc(
                names, status, role, lastNames, status, role
        );
    }

    public Set<User> findByNameOrLastNamesAndNotStatusAndNotRole(String names, String lastNames, String status, ERole role){
        return dao.findByNamesContainingAndStatusNotAndRolesNameNotOrLastNamesContainingAndStatusNotAndRolesNameNotOrderByNamesAscLastNamesAsc(
                names, status, role, lastNames, status, role
        );
    }

    public Optional<User> findByUsername(String username){
        return dao.findByUsername(username);
    }

    public Set<User> findAllCurrentCandidates(String status){
        return dao.findByCandidatesListsStatusOrderByNamesAscLastNamesAsc(status);
    }

    public Set<User> findAllCandidatesByCandidatesList(Integer id){
        return dao.findByCandidatesListsIdOrderByNamesAscLastNamesAsc(id);
    }

    public Set<User> findByName(String names, String lastNames){ return dao.findByNamesContainingAndCandidatesListsStatusOrLastNamesContainingAndCandidatesListsStatusOrderByNamesAscLastNamesAsc(names,"ACT", lastNames,"ACT");}
}
