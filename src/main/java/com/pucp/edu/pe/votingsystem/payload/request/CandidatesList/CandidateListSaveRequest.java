package com.pucp.edu.pe.votingsystem.payload.request.CandidatesList;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

public class CandidateListSaveRequest {

    public static class Candidate{
        private Integer id;

        private String position;

        public Candidate(){

        }

        public Candidate(Integer id, String position){
            this.id = id;
            this.position = position;
        }

        public Integer getId(){
            return  id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }
    }

    @NotBlank
    @Size(max = 120)
    private String name;

    @NotBlank
    @Size(max = 250)
    private String description;

    private String logoUrl;

    @NotBlank
    @Size(max = 3)
    private String status;

    private Integer creationUser;

    private Set<Candidate> users;

    private Integer election;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(Integer creationUser) {
        this.creationUser = creationUser;
    }

    public Set<Candidate> getUsers() {
        return users;
    }

    public void setUsers(Set<Candidate> users) {
        this.users = users;
    }

    public Integer getElection() {
        return election;
    }

    public void setElection(Integer election) {
        this.election = election;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }
}
