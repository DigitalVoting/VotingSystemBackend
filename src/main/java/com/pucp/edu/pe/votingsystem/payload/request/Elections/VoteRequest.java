package com.pucp.edu.pe.votingsystem.payload.request.Elections;

public class VoteRequest {
    private Integer userId;

    private Integer candidateListId;

    private Integer electionId;

    public VoteRequest() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCandidateListId() {
        return candidateListId;
    }

    public void setCandidateListId(Integer candidateListId) {
        this.candidateListId = candidateListId;
    }

    public Integer getElectionId() {
        return electionId;
    }

    public void setElectionId(Integer electionId) {
        this.electionId = electionId;
    }
}
