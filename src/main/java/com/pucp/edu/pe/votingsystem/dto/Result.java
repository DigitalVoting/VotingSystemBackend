package com.pucp.edu.pe.votingsystem.dto;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(	name = "results")
public class Result {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer numberVotes;

    @NotBlank
    @Size(max = 3)
    private String status;

    private Integer creation_user;

    private Integer update_user;

    private LocalDate creation_date;

    private LocalDate update_date;

    @OneToOne
    @JoinTable(	name = "result_election",
            joinColumns = @JoinColumn(name = "result_id"),
            inverseJoinColumns = @JoinColumn(name = "election_id"))
    private Election election;

    @OneToOne
    @JoinTable(	name = "result_candidate_list",
            joinColumns = @JoinColumn(name = "result_id"),
            inverseJoinColumns = @JoinColumn(name = "candidate_list_id"))
    private CandidatesList candidatesList;

    public Result() {
    }

    public Result(Integer numberVotes, String status, Election election, CandidatesList candidatesList, Integer creation_user) {
        this.numberVotes = numberVotes;
        this.status = status;
        this.election = election;
        this.candidatesList = candidatesList;
        this.creation_user = creation_user;
        this.creation_date = LocalDate.now();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumberVotes() {
        return numberVotes;
    }

    public void setNumberVotes(Integer numberVotes) {
        this.numberVotes = numberVotes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCreationUser() {
        return creation_user;
    }

    public void setCreationUser(Integer creation_user) {
        this.creation_user = creation_user;
    }

    public Integer getUpdateUser() {
        return update_user;
    }

    public void setUpdateUser(Integer update_user) {
        this.update_user = update_user;
    }

    public LocalDate getCreationDate() {
        return creation_date;
    }

    public void setCreationDate(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public LocalDate getUpdateDate() {
        return update_date;
    }

    public void setUpdateDate(LocalDate update_date) {
        this.update_date = update_date;
    }

    public Election getElection() {
        return election;
    }

    public void setElection(Election election) {
        this.election = election;
    }

    public CandidatesList getCandidatesList() {
        return candidatesList;
    }

    public void setCandidatesList(CandidatesList candidatesList) {
        this.candidatesList = candidatesList;
    }
}
