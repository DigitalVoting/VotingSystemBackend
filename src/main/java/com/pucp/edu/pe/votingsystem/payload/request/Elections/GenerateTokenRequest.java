package com.pucp.edu.pe.votingsystem.payload.request.Elections;

public class GenerateTokenRequest {

    private Integer userId;

    private Integer electionId;

    public GenerateTokenRequest() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getElectionId() {
        return electionId;
    }

    public void setElectionId(Integer electionId) {
        this.electionId = electionId;
    }
}
