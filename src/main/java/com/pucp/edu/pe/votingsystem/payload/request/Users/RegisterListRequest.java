package com.pucp.edu.pe.votingsystem.payload.request.Users;

import java.text.ParseException;
import java.util.Set;

public class RegisterListRequest {

    public static class User {
        private String username;
        private String names;
        private String lastNames;
        private String email;

        public User() {
        }

        public User(String username, String names, String lastNames, String email) throws ParseException {
            this.username = username;
            this.names = names;
            this.lastNames = lastNames;
            this.email = email;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getNames() {
            return names;
        }

        public void setNames(String names) {
            this.names = names;
        }

        public String getLastNames() {
            return lastNames;
        }

        public void setLastNames(String lastNames) {
            this.lastNames = lastNames;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    private Set<User> users;

    private Integer creationUser;

    private String role;

    public RegisterListRequest() {
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Integer getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(Integer creationUser) {
        this.creationUser = creationUser;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
