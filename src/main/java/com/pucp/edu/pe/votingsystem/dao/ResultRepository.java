package com.pucp.edu.pe.votingsystem.dao;

import com.pucp.edu.pe.votingsystem.dto.Result;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface ResultRepository extends JpaRepository<Result,Integer> {

    Set<Result> findByElectionIdOrderByNumberVotesDesc(Integer electionId);

}
