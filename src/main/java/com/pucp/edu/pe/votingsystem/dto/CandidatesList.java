package com.pucp.edu.pe.votingsystem.dto;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(	name = "candidates_lists",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "name"),
        })
public class CandidatesList {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;

        @NotBlank
        @Size(max = 120)
        private String name;

        @NotBlank
        @Size(max = 250)
        private String description;

        private String logo;

        @NotBlank
        @Size(max = 3)
        private String status;

        private Integer creation_user;

        private Integer update_user;

        private LocalDate creation_date;

        private LocalDate update_date;

        @ManyToMany(fetch = FetchType.LAZY)
        @JoinTable(	name = "candidate_list_elections",
                joinColumns = @JoinColumn(name = "candidate_list_id"),
                inverseJoinColumns = @JoinColumn(name = "election_id"))
        private Set<Election> elections = new HashSet<>();

        @OneToMany(fetch = FetchType.LAZY)
        @JoinTable(	name = "candidate_list_proposal",
                joinColumns = @JoinColumn(name = "candidate_list_id"),
                inverseJoinColumns = @JoinColumn(name = "proposal_id"))
        private Set<Proposal> proposals = new HashSet<>();

        public CandidatesList(){

        }

        public CandidatesList(String name, String description, String logo, String status, Integer creation_user){
                this.name = name;
                this.description = description;
                this.logo = logo;
                this.status = status;
                this.creation_user = creation_user;
                this.creation_date = LocalDate.now();
        }

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getDescription() {
                return description;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public Integer getCreationUser() {
                return creation_user;
        }

        public void setCreationUser(Integer creation_user) {
                this.creation_user = creation_user;
        }

        public Integer getUpdateUser() {
                return update_user;
        }

        public void setUpdateUser(Integer update_user) {
                this.update_user = update_user;
        }

        public LocalDate getCreationDate() {
                return creation_date;
        }

        public void setCreationDate(LocalDate creation_date) {
                this.creation_date = creation_date;
        }

        public LocalDate getUpdateDate() {
                return update_date;
        }

        public void setUpdateDate(LocalDate update_date) {
                this.update_date = update_date;
        }

        public Set<Election> getElections() {
                return elections;
        }

        public void setElections(Set<Election> elections) {
                this.elections = elections;
        }

        public String getLogo() {
                return logo;
        }

        public void setLogo(String logo) {
                this.logo = logo;
        }

        public Set<Proposal> getProposals() {
                return proposals;
        }

        public void setProposals(Set<Proposal> proposals) {
                this.proposals = proposals;
        }
}
