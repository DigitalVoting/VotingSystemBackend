package com.pucp.edu.pe.votingsystem.api;

import com.pucp.edu.pe.votingsystem.dto.Notification;
import com.pucp.edu.pe.votingsystem.payload.request.IdRequest;
import com.pucp.edu.pe.votingsystem.payload.request.NotificationsRequest;
import com.pucp.edu.pe.votingsystem.payload.response.CandidatesSolicitudesResponse;
import com.pucp.edu.pe.votingsystem.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/notification")
public class NotificationApi {
    @Autowired
    NotificationService notificationService;

    @PostMapping("/list-active")
    public ResponseEntity<?> listActiveNotifications(@Valid @RequestBody NotificationsRequest notificationsRequest){

        Set<Notification> personalNotifications = notificationService.findByUserIdOrCandidatesListIdAndStatus(notificationsRequest.getUserId(), notificationsRequest.getCandidateListId(), "ACT");
        Set<Notification> everybodyNotifications = notificationService.findByStatus("ALL");

        ArrayList<Notification> responseList = new ArrayList<>();
        responseList.addAll(personalNotifications);
        responseList.addAll(everybodyNotifications);

        Comparator<Notification> dateComparator = new Comparator<Notification>() {
            public int compare(Notification a, Notification b) {
                return (int) (a.getCreationDate().compareTo(b.getCreationDate()));
            }
        };

        responseList.sort(dateComparator);
        Collections.reverse(responseList);

        return ResponseEntity.ok(responseList);

    }

    @PostMapping("/list-solicitudes")
    public ResponseEntity<?> listSolicitudes(){
        return ResponseEntity.ok(notificationService.findByStatus("SOL"));
    }

    @PostMapping("/candidates-solicitudes")
    public ResponseEntity<?> getCandidatesSolicitudesState(@Valid @RequestBody IdRequest idRequest){

        Set<Notification> solicitudes = notificationService.findByCandidatesListIdAndTitle(idRequest.getId(), "Solicitud de Miembro de Lista");

        CandidatesSolicitudesResponse response = new CandidatesSolicitudesResponse();

        Set<Integer> rejectedCandidates = new HashSet<>();

        Set<Integer> acceptedCandidates = new HashSet<>();

        Set<Integer> pendingCandidates = new HashSet<>();

        String status = "ACCEPTED";

        for (Notification solicitude:
             solicitudes) {
            if(solicitude.getStatus().equals("REJ")){
                rejectedCandidates.add(solicitude.getUser().getId());
                status = "REJECTED";
            }
            else if(solicitude.getStatus().equals("ACC")){
                acceptedCandidates.add(solicitude.getUser().getId());
            }
            else{
                pendingCandidates.add(solicitude.getUser().getId());
                if(!status.equals("REJECTED")) status = "PENDING";
            }
        }

        response.setAcceptedCandidates(acceptedCandidates);
        response.setRejectedCandidates(rejectedCandidates);
        response.setPendingCandidates(pendingCandidates);
        response.setStatus(status);

        return ResponseEntity.ok(response);
    }
}
