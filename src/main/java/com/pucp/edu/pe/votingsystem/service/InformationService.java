package com.pucp.edu.pe.votingsystem.service;

import com.pucp.edu.pe.votingsystem.dao.InformationRepository;
import com.pucp.edu.pe.votingsystem.dto.Information;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class InformationService {
    @Autowired
    private InformationRepository dao;

    public Set<Information> findByUserIdAndCandidatesListId(Integer userId, Integer candidateListId, String status){
        return dao.findByUserIdAndCandidatesListIdAndStatus(userId, candidateListId, status);
    }

    public void saveAndFlush(Information information){
        dao.saveAndFlush(information);
    }
}
