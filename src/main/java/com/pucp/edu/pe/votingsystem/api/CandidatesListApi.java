package com.pucp.edu.pe.votingsystem.api;

import com.pucp.edu.pe.votingsystem.dto.*;
import com.pucp.edu.pe.votingsystem.payload.request.CandidateInformationRequest;
import com.pucp.edu.pe.votingsystem.payload.request.CandidateListProposalRequest;
import com.pucp.edu.pe.votingsystem.payload.request.CandidatesList.AnswerRequest;
import com.pucp.edu.pe.votingsystem.payload.request.CandidatesList.CandidateListSaveRequest;
import com.pucp.edu.pe.votingsystem.payload.request.CandidatesList.CandidateListUpdateRequest;
import com.pucp.edu.pe.votingsystem.payload.request.FindRequest;
import com.pucp.edu.pe.votingsystem.payload.request.IdRequest;
import com.pucp.edu.pe.votingsystem.payload.response.MessageResponse;
import com.pucp.edu.pe.votingsystem.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/candidateslist")
public class CandidatesListApi {
    @Autowired
    private CandidatesListService candidatesListService;

    @Autowired
    private UserService userService;

    @Autowired
    private ElectionService electionService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private RoleService roleService;

    @PostMapping("/save")
    public ResponseEntity<?> registerCandidateList(@Valid @RequestBody CandidateListSaveRequest candidateSaveReq) {
        if (candidatesListService.existsByName(candidateSaveReq.getName())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Lista de candidatos ya existente!"));
        }

        // Create new candidate list
        CandidatesList candidatesList = new CandidatesList(candidateSaveReq.getName(), candidateSaveReq.getDescription(), candidateSaveReq.getLogoUrl(),
                candidateSaveReq.getStatus(), candidateSaveReq.getCreationUser());

        Integer intElection = candidateSaveReq.getElection();
        Set<Election> elections = new HashSet<>();
        if (intElection == null){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: No asignó la lista a una elección!"));
        } else {
            Election candidateListElection = electionService.findById(intElection)
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha elección."));
            elections.add(candidateListElection);
        }

        candidatesList.setElections(elections);
        candidatesListService.saveAndFlush(candidatesList);
        CandidatesList candidatesListC = candidatesListService.findByName(candidatesList.getName()).orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha lista."));

        Set<CandidateListSaveRequest.Candidate> listUsers = candidateSaveReq.getUsers();
        if (listUsers == null) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: No asignó candidatos a la lista!"));
        } else {
            listUsers.forEach(user -> {
                User candidateListUser = userService.findById(user.getId())
                        .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho usuario."));
                candidateListUser.setPosition(user.getPosition());
                Set<CandidatesList> candidatesLists = candidateListUser.getCandidatesLists();
                candidatesLists.add(candidatesListC);
                candidateListUser.setCandidatesLists(candidatesLists);

                Set<Role> roles = new HashSet<>();
                Role role = roleService.findByName(ERole.ROLE_CANDIDATE)
                        .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                roles.add(role);
                if(candidateListUser.getId().equals(candidateSaveReq.getCreationUser())) {
                    Role repRole = roleService.findByName(ERole.ROLE_LIST_ADMIN)
                            .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
                    roles.add(repRole);
                }
                candidateListUser.setRoles(roles);
                userService.save(candidateListUser);

                Notification notification = new Notification();

                notification.setTitle("Solicitud de Miembro de Lista");
                notification.setDescription("Se ha solicitado su integración a la lista "+candidatesList.getName());
                notification.setCreationUser(candidateSaveReq.getCreationUser());
                notification.setCreationDate(LocalDateTime.now());
                notification.setStatus("ACT");
                notification.setCandidatesList(candidatesListC);
                notification.setUser(candidateListUser);

                notificationService.saveAndFlush(notification);
            });

        }

        return ResponseEntity.ok(new MessageResponse("Lista de Candidatos Registrada!"));
    }

    @PostMapping("/listall")
    public Set<CandidatesList> listAllByElection(@RequestParam Integer id){
        return candidatesListService.findAllByElection(id);
    }

    @PostMapping("/listcurrent")
    public Set<CandidatesList> listAllCurrentLists(){
        return candidatesListService.findByElectionsStatusAndStatus("ACT", "ACT");
    }

    @PostMapping("/inactive")
    public Set<CandidatesList> listInactiveLists(@Valid @RequestBody FindRequest findRequest){
        return candidatesListService.findByNameAndNotStatus(findRequest.getName(),"ACT");
    }

    @PostMapping("/candidates")
    public Set<User> listAllCandidates(@Valid @RequestBody CandidateInformationRequest candidateInformationRequest){
        return userService.findAllCandidatesByCandidatesList(candidateInformationRequest.getCandidateListId());
    }

    @PostMapping("/find")
    public ResponseEntity<?> findCandidatesLists(@Valid @RequestBody FindRequest findRequest){
        return ResponseEntity.ok(candidatesListService.findByNameAndStatus(findRequest.getName(), "ACT"));
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateCandidateList(@Valid @RequestBody CandidateListUpdateRequest candidateListUpdateRequest){

        CandidatesList candidatesList = candidatesListService.findById(candidateListUpdateRequest.getCandidatesListId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha Lista de Candidatos."));

        User user = userService.findById(candidateListUpdateRequest.getCandidatesListId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Usuario."));

        if(candidateListUpdateRequest.getType().equals("sol")){

            candidatesList.setName(candidateListUpdateRequest.getName());
            candidatesList.setDescription(candidateListUpdateRequest.getDescription());
            if(candidateListUpdateRequest.getLogoUrl()!=null && !candidateListUpdateRequest.getLogoUrl().isEmpty())
                candidatesList.setLogo(candidateListUpdateRequest.getLogoUrl());
            candidatesList.setUpdateDate(LocalDate.now());
            candidatesList.setUpdateUser(candidateListUpdateRequest.getUpdateUser());
            candidatesList.setStatus(candidateListUpdateRequest.getStatus());

            candidatesListService.saveAndFlush(candidatesList);

            Notification newNotification = new Notification();

            newNotification.setTitle("Solicitud de Validación");
            newNotification.setDescription("Se solicita la validación de la lista de "+candidatesList.getName());
            newNotification.setCreationUser(candidateListUpdateRequest.getUpdateUser());
            newNotification.setCreationDate(LocalDateTime.now());
            newNotification.setStatus("SOL");
            newNotification.setCandidatesList(candidatesList);
            newNotification.setUser(user);

            notificationService.saveAndFlush(newNotification);

        }
        else{

            candidatesList.setStatus(candidateListUpdateRequest.getStatus());
            candidatesListService.saveAndFlush(candidatesList);

            Notification notification = notificationService.findById(candidateListUpdateRequest.getNotificationId())
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha Notificación."));

            notification.setStatus("ACT");
            notification.setUpdateDate(LocalDateTime.now());
            notification.setUpdateUser(user.getId());

            if(candidateListUpdateRequest.getStatus().equals("ACT")){
                notification.setDescription("Se ha aceptado su solicitud para que su lista forme parte de las elecciones");
            }
            else {
                notification.setDescription("Se ha rechazado su solicitud para que su lista forme parte de las elecciones");
            }

            notificationService.saveAndFlush(notification);

        }

        return ResponseEntity.ok(new MessageResponse("Actualización Realizada Exitosamente!"));
    }

    @PostMapping("/answer-solicitude")
    public ResponseEntity<?> answerSolicitude(@Valid @RequestBody AnswerRequest answerRequest){

        Notification notification = notificationService.findById(answerRequest.getNotificationId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha notificacion."));

        User user = userService.findById(answerRequest.getUserId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho usuario."));

        CandidatesList candidatesList = candidatesListService.findById(answerRequest.getCandidatesListId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha lista."));

        if(answerRequest.getAnswer()){

            notification.setStatus("ACC");
            notificationService.saveAndFlush(notification);

            Notification newNotification = new Notification();

            newNotification.setTitle("Solicitud Aceptada");
            newNotification.setDescription("Se ha aceptado la integración a la lista de "+user.getNames()+" "+user.getLastNames());
            newNotification.setCreationUser(answerRequest.getUserId());
            newNotification.setCreationDate(LocalDateTime.now());
            newNotification.setStatus("ACT");
            newNotification.setCandidatesList(candidatesList);
            newNotification.setUser(user);

            notificationService.saveAndFlush(newNotification);

        }
        else {

            Set<Role> roles = new HashSet<>();
            Role role = roleService.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho Rol."));
            roles.add(role);
            user.setRoles(roles);

            Set<CandidatesList> candidatesLists = new HashSet<>();
            for (CandidatesList cl:
                 user.getCandidatesLists()) {
                if(!cl.getId().equals(answerRequest.getCandidatesListId())) candidatesLists.add(cl);
            }
            user.setCandidatesLists(candidatesLists);

            userService.save(user);

            notification.setStatus("REJ");
            notificationService.saveAndFlush(notification);

            Notification newNotification = new Notification();

            newNotification.setTitle("Solicitud Rechazada");
            newNotification.setDescription("Se ha rechazado la integración a la lista de "+user.getNames()+" "+user.getLastNames());
            newNotification.setCreationUser(answerRequest.getUserId());
            newNotification.setCreationDate(LocalDateTime.now());
            newNotification.setStatus("ACT");
            newNotification.setCandidatesList(candidatesList);
            newNotification.setUser(user);

            notificationService.saveAndFlush(newNotification);

        }

        return ResponseEntity.ok(new MessageResponse("Respondido exitosamente!"));
    }

}
