package com.pucp.edu.pe.votingsystem.api;

import com.pucp.edu.pe.votingsystem.dto.*;
import com.pucp.edu.pe.votingsystem.payload.request.Elections.ConfirmTokenRequest;
import com.pucp.edu.pe.votingsystem.payload.request.Elections.GenerateTokenRequest;
import com.pucp.edu.pe.votingsystem.payload.request.Elections.RegisterElectionRequest;
import com.pucp.edu.pe.votingsystem.payload.request.Elections.VoteRequest;
import com.pucp.edu.pe.votingsystem.payload.request.FindRequest;
import com.pucp.edu.pe.votingsystem.payload.request.IdRequest;
import com.pucp.edu.pe.votingsystem.payload.request.LoginRequest;
import com.pucp.edu.pe.votingsystem.payload.request.Users.RegisterListRequest;
import com.pucp.edu.pe.votingsystem.payload.response.ActiveElection;
import com.pucp.edu.pe.votingsystem.payload.response.MessageResponse;
import com.pucp.edu.pe.votingsystem.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.io.IOException;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/election")
public class ElectionApi {
    @Autowired
    ElectionService electionService;

    @Autowired
    UserService userService;

    @Autowired
    VoteService voteService;

    @Autowired
    CandidatesListService candidatesListService;

    @Autowired
    ResultService resultService;

    @Autowired
    PasswordEncoder encoder;

    @Value("${spring.mail.username}")
    private String mailUsername;

    @Value("${spring.mail.password}")
    private String mailPassword;

    @PostMapping("/active-election")
    public ResponseEntity<?> listActiveElection(){
        ArrayList<Election> electionOpt = electionService.findActiveElection();
        if(electionOpt.get(0) != null){
            Election election = electionOpt.get(0);
            ActiveElection response = new ActiveElection();
            response.setId(election.getId());
            response.setTitle(election.getName());
            response.setBanner(election.getLogo());

            LocalDateTime fecha = LocalDateTime.now();
            LocalDateTime startDateTime = LocalDateTime.now();
            LocalDateTime endDateTime = LocalDateTime.now();
            String startDate = "";
            String endDate = "";
            if(fecha.compareTo(election.getStartDateInscriptions())>=0 && fecha.compareTo(election.getEndDateInscriptions())<0){
                response.setStage("inscriptions");
                response.setInformation(election.getDescriptionInscriptions());
                startDateTime = election.getStartDateInscriptions();
                endDateTime = election.getEndDateInscriptions();
            }
            else if(fecha.compareTo(election.getStartDateActualization())>=0 && fecha.compareTo(election.getEndDateActualization())<0){
                response.setStage("actualization");
                response.setInformation(election.getDescriptionActualization());
                startDateTime = election.getStartDateActualization();
                endDateTime = election.getEndDateActualization();
            }
            else if(fecha.compareTo(election.getStartDateVote())>=0 && fecha.compareTo(election.getEndDateVote())<0){
                response.setStage("vote");
                response.setInformation(election.getDescriptionVote());
                startDateTime = election.getStartDateVote();
                endDateTime = election.getEndDateVote();
            }
            else if(fecha.compareTo(election.getStartDateInscriptions())<0){
                response.setStage("waiting");
                response.setInformation(election.getDescription());
                startDateTime = election.getStartDateInscriptions();
                endDateTime = election.getEndDateVote();
            }
            else{
                response.setStage("results");
                response.setInformation(election.getDescription());
                startDateTime = election.getStartDateInscriptions();
                endDateTime = election.getEndDateVote();
            }
            //Formato "18/07/2018 08:00 horas"
            startDate = startDate.concat(startDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            startDate = startDate.concat(" horas");

            endDate = endDate.concat(endDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            endDate = endDate.concat(" horas");

            response.setStartDate(startDate);
            response.setEndDate(endDate);

            return ResponseEntity.ok(response);
        }
        else{
            return ResponseEntity.ok(new MessageResponse("No se encuentra alguna elección activa"));
        }
    }

    @PostMapping("/active-election-stage")
    public ResponseEntity<?> getActiveElectionStage(){
        ArrayList<Election> electionOpt = electionService.findActiveElection();
        if(electionOpt.get(0) != null) {
            Election election = electionOpt.get(0);
            LocalDateTime fecha = LocalDateTime.now();
            if (fecha.compareTo(election.getStartDateInscriptions()) >= 0 && fecha.compareTo(election.getEndDateInscriptions()) < 0) {
                return ResponseEntity.ok("inscriptions");
            } else if (fecha.compareTo(election.getStartDateActualization()) >= 0 && fecha.compareTo(election.getEndDateActualization()) < 0) {
                return ResponseEntity.ok("actualization");
            } else if (fecha.compareTo(election.getStartDateVote()) >= 0 && fecha.compareTo(election.getEndDateVote()) < 0) {
                return ResponseEntity.ok("vote");
            } else if (fecha.compareTo(election.getStartDateInscriptions()) < 0) {
                return ResponseEntity.ok("waiting");
            } else {
                return ResponseEntity.ok("results");
            }
        }
        else{
            return ResponseEntity.ok("results");
        }
    }

    @PostMapping("election-stage")
    public ResponseEntity<?> getElectionStage(@RequestBody @Valid IdRequest idRequest){
        Optional<Election> electionOpt = electionService.findById(idRequest.getId());
        if(electionOpt.isPresent()) {
            Election election = electionOpt.get();
            LocalDateTime fecha = LocalDateTime.now();
            if (fecha.compareTo(election.getStartDateInscriptions()) >= 0 && fecha.compareTo(election.getEndDateInscriptions()) < 0) {
                return ResponseEntity.ok("inscriptions");
            } else if (fecha.compareTo(election.getStartDateActualization()) >= 0 && fecha.compareTo(election.getEndDateActualization()) < 0) {
                return ResponseEntity.ok("actualization");
            } else if (fecha.compareTo(election.getStartDateVote()) >= 0 && fecha.compareTo(election.getEndDateVote()) < 0) {
                return ResponseEntity.ok("vote");
            } else if (fecha.compareTo(election.getStartDateInscriptions()) < 0) {
                return ResponseEntity.ok("waiting");
            } else {
                return ResponseEntity.ok("results");
            }
        }
        else{
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Id incorrecto"));
        }
    }

    @PostMapping("find-actives")
    public ResponseEntity<?> getActiveElections(@RequestBody @Valid FindRequest findRequest){
        return ResponseEntity.ok(electionService.findbyNameAndNotStatus(findRequest.getName(),"INA"));
    }

    @PostMapping("/find-inactives")
    public ResponseEntity<?> getInactiveElections(@RequestBody @Valid FindRequest findRequest){
        return ResponseEntity.ok(electionService.findbyNameAndNotStatus(findRequest.getName(), "ACT"));
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateElection(@RequestBody @Valid RegisterElectionRequest registerElectionRequest){

        Optional<Election> existentElection = electionService.findbyName(registerElectionRequest.getName());

        if(existentElection.isPresent()){

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            Election election = existentElection.get();

            if(!registerElectionRequest.getDescription().equals("")){
                election.setDescription(registerElectionRequest.getDescription());
            }
            if(!registerElectionRequest.getLogo().equals("")){
                election.setLogo(registerElectionRequest.getLogo());
            }

            election.setUpdateUser(registerElectionRequest.getCreationUser());
            election.setUpdateDate(LocalDate.now());

            if(!registerElectionRequest.getDescriptionInscriptions().equals("")){
                election.setDescriptionInscriptions(registerElectionRequest.getDescriptionInscriptions());
            }
            if(!registerElectionRequest.getStartDateInscriptions().equals("")){
                election.setStartDateInscriptions(LocalDateTime.parse(registerElectionRequest.getStartDateInscriptions(), formatter));
            }
            if(!registerElectionRequest.getEndDateInscriptions().equals("")){
                election.setEndDateInscriptions(LocalDateTime.parse(registerElectionRequest.getEndDateInscriptions(), formatter));
            }

            if(!registerElectionRequest.getDescriptionActualization().equals("")){
                election.setDescriptionActualization(registerElectionRequest.getDescriptionActualization());
            }
            if(!registerElectionRequest.getDescription().equals("")){
                election.setStartDateActualization(LocalDateTime.parse(registerElectionRequest.getStartDateActualization(), formatter));
            }
            if(!registerElectionRequest.getEndDateActualization().equals("")){
                election.setEndDateActualization(LocalDateTime.parse(registerElectionRequest.getEndDateActualization(), formatter));
            }

            if(!registerElectionRequest.getDescriptionVote().equals("")){
                election.setDescriptionVote(registerElectionRequest.getDescriptionVote());
            }
            if(!registerElectionRequest.getStartDateVote().equals("")){
                election.setStartDateVote(LocalDateTime.parse(registerElectionRequest.getStartDateVote(), formatter));
            }
            if(!registerElectionRequest.getEndDateVote().equals("")){
                election.setEndDateVote(LocalDateTime.parse(registerElectionRequest.getEndDateVote(), formatter));
            }

            return ResponseEntity.ok(electionService.saveAndFlush(election));
        }
        else{
            return ResponseEntity.badRequest().body(new MessageResponse("Error: No se encontró la elección."));
        }
    }

    private void sendmail(String emailRecepient, String subject, String content) throws AddressException, MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailUsername, mailPassword);
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(mailUsername, false));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailRecepient));
        msg.setSubject(subject);
        msg.setContent(content, "text/html");
        msg.setSentDate(new Date());

        Transport.send(msg);
    }

    @PostMapping("/generate-token")
    public ResponseEntity<?> registerUser(@Valid @RequestBody GenerateTokenRequest generateTokenRequest) {

        User user = userService.findById(generateTokenRequest.getUserId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicho usuario."));

        Optional<Vote> vote = voteService.findByUserIdAndStatusAndElectionId(generateTokenRequest.getUserId(), "PEN", generateTokenRequest.getElectionId());

        Random r = new Random( System.currentTimeMillis() );
        int number = ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
        String token = encoder.encode(Integer.toString(number));

        if(vote.isPresent()){
            Vote existentVote = vote.get();

            existentVote.setToken(token);

            voteService.saveAndFlush(existentVote);
        }
        else{
            Vote newVote = new Vote();

            newVote.setToken(token);

            Election election = electionService.findById(generateTokenRequest.getElectionId())
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha elección."));

            newVote.setElection(election);
            newVote.setUser(user);
            newVote.setCreationUser(generateTokenRequest.getUserId());
            newVote.setCreationDate(LocalDate.now());
            newVote.setStatus("PEN");

            voteService.saveAndFlush(newVote);
        }

        try{
            sendmail(user.getEmail(),"Token de Verificación", "Su token es: "+number);
        }
        catch (Exception e){
            ResponseEntity.ok(e);
        }

        return ResponseEntity.ok(new MessageResponse("Token Enviado Exitosamente"));

    }

    @PostMapping("/has-voted")
    public ResponseEntity<?> hasVoted(@Valid @RequestBody GenerateTokenRequest generateTokenRequest) {

        Optional<Vote> vote = voteService.findByUserIdAndStatusAndElectionId(generateTokenRequest.getUserId(), "ACT", generateTokenRequest.getElectionId());

        if(vote.isPresent()){
            return ResponseEntity.ok(1);
        }
        else{
            return ResponseEntity.ok(0);
        }
    }

    @PostMapping("/vote")
    public ResponseEntity<?> vote(@Valid @RequestBody VoteRequest voteRequest) {

        Optional<Vote> vote = voteService.findByUserIdAndStatusAndElectionId(voteRequest.getUserId(), "ACT", voteRequest.getElectionId());

        if(vote.isPresent()){
            return ResponseEntity.ok(0);
        }
        else{
            Vote voteRegister = voteService.findByUserIdAndStatusAndElectionId(voteRequest.getUserId(), "PEN", voteRequest.getElectionId())
                    .orElseThrow(() -> new RuntimeException("Error: No se encuentra el voto del elector."));


            voteRegister.setStatus("ACT");

            return ResponseEntity.ok(1);
        }
    }

    @PostMapping("/confirm-token")
    public ResponseEntity<?> confirmToken(@Valid @RequestBody ConfirmTokenRequest confirmTokenRequest) {

        Vote voteRegister = voteService.findByUserIdAndStatusAndElectionId(confirmTokenRequest.getUserId(), "PEN", confirmTokenRequest.getElectionId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra el voto del elector."));

        if(encoder.matches(confirmTokenRequest.getToken(), voteRegister.getToken())){
            return ResponseEntity.ok(1);
        }
        else{
            return ResponseEntity.ok(0);
        }

    }

    @PostMapping("/generate-results")
    public ResponseEntity<?> calculateResults(@Valid @RequestBody GenerateTokenRequest generateTokenRequest) {

        Set<Vote> votes = voteService.findByElectionIdAndStatus(generateTokenRequest.getElectionId(), "ACT");

        Set<CandidatesList> candidatesLists = candidatesListService.findByElectionsStatusAndStatus("ACT","ACT");

        Election election = electionService.findById(generateTokenRequest.getElectionId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra la elección activa."));

        HashSet<Result> results = new HashSet<>();

        for (CandidatesList candidatesList :
                candidatesLists) {
            Result result = new Result();
            result.setNumberVotes(0);
            result.setStatus("ACT");
            result.setElection(election);
            result.setCandidatesList(candidatesList);
            result.setCreationUser(generateTokenRequest.getUserId());
            result.setCreationDate(LocalDate.now());

            results.add(result);
        }

        for (Vote vote :
                votes) {
            for (Result result :
                    results) {
                if(encoder.matches(result.getCandidatesList().getId().toString(),vote.getVote())){
                    result.setNumberVotes(result.getNumberVotes()+1);
                    break;
                }
            }
        }

        resultService.saveResults(results);

        return ResponseEntity.ok(new MessageResponse("Resultados Generados Exitosamente"));

    }

    @PostMapping("/results")
    public ResponseEntity<?> getResults(@Valid @RequestBody IdRequest idRequest) {
        return ResponseEntity.ok(resultService.findByElectionIdOrderByNumberVotes(idRequest.getId()));
    }

    @PostMapping("/election")
    public ResponseEntity<?> registerElection(@Valid @RequestBody RegisterElectionRequest registerElectionRequest){

        Optional<Election> existentElection = electionService.findbyName(registerElectionRequest.getName());

        if(existentElection.isPresent()){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: El nombre de elección ya se encuentra registrado."));
        }
        else{

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            Election election = new Election(registerElectionRequest.getName(), registerElectionRequest.getDescription(), registerElectionRequest.getLogo(),
                    LocalDateTime.parse(registerElectionRequest.getStartDateInscriptions(), formatter), LocalDateTime.parse(registerElectionRequest.getEndDateInscriptions(), formatter),
                    LocalDateTime.parse(registerElectionRequest.getStartDateActualization(), formatter), LocalDateTime.parse(registerElectionRequest.getEndDateActualization(), formatter),
                    LocalDateTime.parse(registerElectionRequest.getStartDateVote(), formatter), LocalDateTime.parse(registerElectionRequest.getEndDateVote(), formatter),
                    registerElectionRequest.getStatus(), registerElectionRequest.getCreationUser(), registerElectionRequest.getDescriptionInscriptions(),
                    registerElectionRequest.getDescriptionActualization(), registerElectionRequest.getDescriptionVote());

            return ResponseEntity.ok(electionService.saveAndFlush(election));
        }

    }
}
