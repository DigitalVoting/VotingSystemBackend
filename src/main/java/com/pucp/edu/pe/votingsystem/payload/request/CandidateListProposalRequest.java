package com.pucp.edu.pe.votingsystem.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

public class CandidateListProposalRequest {
    private Integer candidateListId;

    private Integer creationUserId;

    public static class ProposalAdd{
        private String name;

        private String description;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    private Set<ProposalAdd> proposals;

    public Integer getCandidateListId() {
        return candidateListId;
    }

    public void setCandidateListId(Integer candidateListId) {
        this.candidateListId = candidateListId;
    }

    public Set<ProposalAdd> getProposals() {
        return proposals;
    }

    public void setProposals(Set<ProposalAdd> proposals) {
        this.proposals = proposals;
    }

    public Integer getCreationUserId() {
        return creationUserId;
    }

    public void setCreationUserId(Integer creationUserId) {
        this.creationUserId = creationUserId;
    }
}
