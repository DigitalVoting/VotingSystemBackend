package com.pucp.edu.pe.votingsystem.payload.request.CandidatesList;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CandidateListUpdateRequest {

    private Integer candidatesListId;

    private String name;

    private String description;

    private String logoUrl;

    private String status;

    private Integer updateUser;

    private String type;

    private Integer notificationId;

    public CandidateListUpdateRequest() {
    }

    public Integer getCandidatesListId() {
        return candidatesListId;
    }

    public void setCandidatesListId(Integer candidatesListId) {
        this.candidatesListId = candidatesListId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Integer updateUser) {
        this.updateUser = updateUser;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }
}
