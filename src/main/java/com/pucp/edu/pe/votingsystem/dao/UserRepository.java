package com.pucp.edu.pe.votingsystem.dao;

import com.pucp.edu.pe.votingsystem.dto.CandidatesList;
import com.pucp.edu.pe.votingsystem.dto.ERole;
import com.pucp.edu.pe.votingsystem.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface UserRepository extends JpaRepository<User,Integer> {
    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Set<User> findByCandidatesListsStatusOrderByNamesAscLastNamesAsc(String status);

    Set<User> findByCandidatesListsIdOrderByNamesAscLastNamesAsc(Integer id);

    Set<User> findByStatus(String status);

    Set<User> findByNamesContainingAndStatusOrLastNamesContainingAndStatusOrderByNamesAscLastNamesAsc(String names, String status1, String lastNames, String status2);

    Set<User> findByNamesContainingAndCandidatesListsStatusOrLastNamesContainingAndCandidatesListsStatusOrderByNamesAscLastNamesAsc(String names, String status1, String lastNames, String status2);

    Set<User> findByNamesContainingAndStatusAndRolesNameOrLastNamesContainingAndStatusAndRolesNameOrderByNamesAscLastNamesAsc(String names, String status1, ERole role1, String lastNames, String status2, ERole role2);

    Set<User> findByNamesContainingAndStatusAndRolesNameNotOrLastNamesContainingAndStatusAndRolesNameNotOrderByNamesAscLastNamesAsc(String names, String status1, ERole role1, String lastNames, String status2, ERole role2);

    Set<User> findByNamesContainingAndStatusNotAndRolesNameNotOrLastNamesContainingAndStatusNotAndRolesNameNotOrderByNamesAscLastNamesAsc(String names, String status1, ERole role1, String lastNames, String status2, ERole role2);
}
