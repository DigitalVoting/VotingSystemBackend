package com.pucp.edu.pe.votingsystem.dao;

import com.pucp.edu.pe.votingsystem.dto.Banner;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface BannerRepository extends JpaRepository<Banner,Integer> {
    Set<Banner> findAllByStatus(String status);
}
