package com.pucp.edu.pe.votingsystem.service;

import com.pucp.edu.pe.votingsystem.dao.ProposalRepository;
import com.pucp.edu.pe.votingsystem.dto.Proposal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProposalService {
    @Autowired
    private ProposalRepository dao;

    public Optional<Proposal> findById(Integer id){
        return dao.findById(id);
    }

    public void saveAndFlush(Proposal proposal){ dao.saveAndFlush(proposal);}
}
