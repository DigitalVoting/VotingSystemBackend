package com.pucp.edu.pe.votingsystem.api;

import com.pucp.edu.pe.votingsystem.dto.*;
import com.pucp.edu.pe.votingsystem.payload.request.CandidateInformationRequest;
import com.pucp.edu.pe.votingsystem.payload.request.SaveProposalsRequest;
import com.pucp.edu.pe.votingsystem.payload.response.MessageResponse;
import com.pucp.edu.pe.votingsystem.service.CandidatesListService;
import com.pucp.edu.pe.votingsystem.service.NotificationService;
import com.pucp.edu.pe.votingsystem.service.ProposalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/proposal")
public class ProposalApi {
    @Autowired
    private ProposalService proposalService;

    @Autowired
    private CandidatesListService candidatesListService;

    @Autowired
    private NotificationService notificationService;

    @PostMapping("/update")
    public ResponseEntity<?> updateProposals(@Valid @RequestBody SaveProposalsRequest saveProposalsRequest){

        CandidatesList candidatesList = candidatesListService.findById(saveProposalsRequest.getCandidateListId())
                .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha lista de candidatos."));

        Set<Proposal> proposalsAdded = candidatesList.getProposals();

        for (Proposal proposal:
                saveProposalsRequest.getProposals()) {

            if(proposal.getId()!=null && proposal.getId()!=0){
                Proposal existentProposal = proposalService.findById(proposal.getId())
                        .orElseThrow(() -> new RuntimeException("Error: No se encuentra dicha propuesta."));

                existentProposal.setDescription(proposal.getDescription());
                existentProposal.setName(proposal.getName());
                existentProposal.setUpdateDate(LocalDate.now());
                existentProposal.setUpdateUser(saveProposalsRequest.getCreationUserId());
                proposalService.saveAndFlush(existentProposal);
            }
            else{
                Proposal newProposal = new Proposal(proposal.getName(),proposal.getDescription(),
                        "ACT",saveProposalsRequest.getCreationUserId());

                proposalsAdded.add(newProposal);

                proposalService.saveAndFlush(newProposal);
            }
        }
        candidatesList.setProposals(proposalsAdded);

        candidatesListService.saveAndFlush(candidatesList);

        if(candidatesList.getStatus().equals("ACT")){
            Notification notification = new Notification();

            notification.setTitle("Actualización de Información");
            notification.setDescription(candidatesList.getName()+" realizó la actualización de sus datos");
            notification.setCreationUser(saveProposalsRequest.getCreationUserId());
            notification.setCreationDate(LocalDateTime.now());
            notification.setStatus("ALL");
            notification.setCandidatesList(candidatesList);

            notificationService.saveAndFlush(notification);
        }

        return ResponseEntity.ok(new MessageResponse("Actualización Realizada Exitosamente!"));
    }
}
